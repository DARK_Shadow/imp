program imp_test;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  System.SysUtils,
  System.RegularExpressions,
  System.RegularExpressionsCore,
  System.Rtti,
  uAst.Interfaces in 'src\uAst.Interfaces.pas',
  uAst in 'src\uAst.pas',
  uCombinators.Interfaces in 'src\uCombinators.Interfaces.pas',
  uCombinators in 'src\uCombinators.pas',
  uGenericsComparer in 'src\uGenericsComparer.pas',
  uImp.Parser.Values in 'src\uImp.Parser.Values.pas',
  uImp_Ast in 'src\uImp_Ast.pas',
  uImp_Combinators in 'src\uImp_Combinators.pas',
  uImp_Lexer in 'src\uImp_Lexer.pas',
  uImp_Parser in 'src\uImp_Parser.pas',
  uLexer in 'src\uLexer.pas',
  uValues in 'src\uValues.pas',
  uImp_Ast.ExecutionEngine in 'src\uImp_Ast.ExecutionEngine.pas',
  uParserTreeForm in 'src\uParserTreeForm.pas' {ParserTreeForm},
  uCombinators.IInput in 'src\uCombinators.IInput.pas',
  uCombinators.Input in 'src\uCombinators.Input.pas',
  uCombinators.IResult in 'src\uCombinators.IResult.pas',
  uCombinators.Result in 'src\uCombinators.Result.pas',
  uArrayHelper in 'src\uArrayHelper.pas';

procedure Run_Test;
const
  LText: string =
    'n := 4;' + sLineBreak +
    'p := 1;' + sLineBreak +
    'while n > 1 do' + sLineBreak +
    '  p := p * n;' + sLineBreak +
    '  n := n - 1' + sLineBreak +
    'end';

var
  LTokens: TArray<TToken>;
  LResult: IResult;
  LEnv: IEnvironment;
  LExecutor: IExecutionEngine;
begin
  LTokens := Imp_Lex(LText,
    procedure(const AError: string)
    begin
      Writeln('Error: ', AError);
    end);
  LResult := Parser[TValue.From<TArray<TToken>>(LTokens)];

  LEnv := TEnvironment.Create;
  LExecutor := TExecutor.Create;
  LExecutor.Compile(LResult.Value.AsType<IStatement>);

  LEnv.Clean;
  LExecutor.Execute(LEnv);
  Writeln(LEnv.Value['n'].ToString);
  Writeln(LEnv.Value['p'].ToString);

  LEnv.Clean;
  LExecutor.Execute(LEnv);
  Writeln(LEnv.Value['n'].ToString);
  Writeln(LEnv.Value['p'].ToString);

  (procedure()
  var
    n: Integer;
    p: Integer;
  begin
    n := 4;
    p := 1;
    while n > 1 do
    begin
      p := p * n;
      n := n - 1;
    end;
    Writeln(n);
    Writeln(p);
  end)();

//  (procedure(const AForm: TParserTreeForm)
//  begin
//    try
//      AForm.Combinator := Parser;
//      AForm.ShowModal;
//    finally
//      AForm.Free;
//    end;
//  end)(TParserTreeForm.Create(nil));

end;

begin
  ReportMemoryLeaksOnShutdown := True;
  try
    Run_Test;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
  Writeln('Press any key...');
  Readln;
end.
