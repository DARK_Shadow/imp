unit Test_uArrayHelper;

interface

uses
  TestFramework;

type
  TTestArrayHelper = class(TTestCase)
  published
    procedure TestAggregate;
    procedure TestSelect;
  end;


implementation

uses
  System.SysUtils,
  System.Generics.Collections,
  uArrayHelper;

{ TTestArrayHelper }

procedure TTestArrayHelper.TestAggregate;
var
  LSource: TArray<Integer>;
begin
  LSource := TArray<Integer>.Create(1, 2, 3, 4, 5, 6, 7, 8, 9);

  CheckEquals(45, TArray.
    Aggregate<Integer,Integer>(LSource, TFunctionOf<Integer>.Identity,
      function(AResult, AItem: Integer): Integer
      begin
        Result := AResult + AItem;
      end));

  CheckEquals('123456789', TArray.
    Aggregate<Integer,String>(LSource, IntToStr,
      function(AResult, AItem: string): string
      begin
        Result := AResult + AItem
      end));
end;

procedure TTestArrayHelper.TestSelect;
var
  LSource: TArray<Integer>;
  LStrings: TArray<string>;
begin
  LSource := TArray<Integer>.Create(1, 2, 3, 4, 5, 6, 7, 8, 9);

  LStrings := TArray.Select<Integer,string>(LSource, IntToStr);

  CheckEquals('1|2|3|4|5|6|7|8|9',
    TArray.Aggregate<string,string>(LStrings, TFunctionOf<string>.Identity,
      function(AResult, AItem: string): string
      begin
        Result := AResult + '|' + AItem;
      end));
end;

initialization
  RegisterTest(TTestArrayHelper.Suite);

end.
