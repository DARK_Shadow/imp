unit Test_uParser;

interface

uses
  TestFramework,
  System.Rtti,
  uAst.Interfaces,
  uImp_Combinators,
  uLexer,
  uImp_Ast;

type
  TTestParser = class(TTestCase)
  private
    procedure ParserTest(const ACode: string; const AParser: TParser; const AExprected: INode); overload;
    procedure ParserTest(const ACode: string; const AParser: TParser; const AExprected: TValue); overload;
    function OnErrorBuilder(const ATest: TTestCase): TOnError;
  published
    procedure TestAssignStatement;
    procedure TestCompoundStatement;
    procedure TestPrecedence;

    procedure TestAExpNum;
    procedure TestAExpVar;
    procedure TestAExpGroup;
    procedure TestAExpBinOp;
    procedure TestAExpBinOpGroup;

    procedure TestBExpRelationOperator;
    procedure TestBExpNot;
    procedure TestBExpAnd;
    procedure TestBExpLogic;
    procedure TestBExpLogicGroup;
    procedure TestBExpNotPrecedence;

    procedure TestIfStatement;
    procedure TestWhileStatement;
  end;

implementation

uses
  System.SysUtils,
  System.Generics.Collections,
  uCombinators.Interfaces,
  uCombinators.IInput,
  uCombinators.IResult,
  uImp_Lexer,
  uImp_Parser;

{ TTestParser }

function TTestParser.OnErrorBuilder(const ATest: TTestCase): TOnError;
begin
  Result :=
    procedure(const AError: string)
    begin
      ATest.Status(AError);
    end;
end;

procedure TTestParser.ParserTest(const ACode: string; const AParser: TParser; const AExprected: INode);
var
  LTokens: TArray<TValue>;
  LResult: IResult;
  LToken: TToken;
begin
  with TList<TValue>.Create do
  try
    for LToken in Imp_Lex(ACode, OnErrorBuilder(Self)) do
      Add(TValue.From<TToken>(LToken));

    LTokens := ToArray;
  finally
    Free;
  end;
  LResult := TParserFunc(AParser)(TInput.Build(LTokens));
  CheckEquals(Assigned(AExprected), LResult.WasSuccessful, LResult.ToString);
  if LResult.WasSuccessful then
    CheckEquals(AExprected.ToString, INode(LResult.Value.AsInterface).ToString);
end;

procedure TTestParser.ParserTest(const ACode: string; const AParser: TParser; const AExprected: TValue);
var
  LTokens: TArray<TValue>;
  LResult: IResult;
  LToken: TToken;
begin
  with TList<TValue>.Create do
  try
    for LToken in Imp_Lex(ACode, OnErrorBuilder(Self)) do
      Add(TValue.From<TToken>(LToken));

    LTokens := ToArray;
  finally
    Free;
  end;

  LResult := TParserFunc(AParser)(TInput.Build(LTokens));
  CheckEquals(not AExprected.IsEmpty, LResult.WasSuccessful, 'Result is nil');
  if LResult.WasSuccessful then
    CheckEquals(AExprected.ToString, LResult.Value.ToString);
end;

procedure TTestParser.TestAExpBinOp;
var
  LExpected: INode;
begin
  with TNodeFactory do
    LExpected :=
      BinOpAExp('+',
        BinOpAExp('*',
          IntegerAExp(2),
          IntegerAExp(3)),
        IntegerAExp(4));
  ParserTest('2 * 3 + 4', AExp, LExpected);
end;

procedure TTestParser.TestAExpBinOpGroup;
var
  LExpected: INode;
begin
  with TNodeFactory do
    LExpected :=
      BinOpAExp('*',
        IntegerAExp(2),
          BinOpAExp('+',
            IntegerAExp(3),
            IntegerAExp(4)));
  ParserTest('2 * (3 + 4)', AExp, LExpected);
end;

procedure TTestParser.TestAExpGroup;
begin
  ParserTest('(12)', AExp, TNodeFactory.IntegerAExp(12));
end;

procedure TTestParser.TestAExpNum;
begin
  ParserTest('12', AExp, TNodeFactory.IntegerAExp(12));
end;

procedure TTestParser.TestAExpVar;
begin
  ParserTest('x', AExp, TNodeFactory.VariableAExp('x'));
end;

procedure TTestParser.TestAssignStatement;
var
  LAst: INode;
begin
  LAst := TNodeFactory.AssignStatement('x', TNodeFactory.IntegerAExp(1));
  ParserTest('x := 1', Statements_List, LAst);
end;

procedure TTestParser.TestBExpAnd;
var
  LAst: INode;
begin
  with TNodeFactory do
    LAst := AndBExp(RelOpBExp('<', IntegerAExp(2), IntegerAExp(3)),
      RelOpBExp('<', IntegerAExp(3), IntegerAExp(4)));
  ParserTest('2 < 3 and 3 < 4', BExp, LAst);
end;

procedure TTestParser.TestBExpLogic;
var
  LExpected: INode;
begin
  with TNodeFactory do
    LExpected := OrBExp(
      AndBExp(
        RelOpBExp('<', IntegerAExp(1), IntegerAExp(2)),
        RelOpBExp('<', IntegerAExp(3), IntegerAExp(4))),
      RelOpBExp('<', IntegerAExp(5), IntegerAExp(6)));
  ParserTest('1 < 2 and 3 < 4 or 5 < 6', BExp, LExpected);
end;

procedure TTestParser.TestBExpLogicGroup;
var
  LExpected: INode;
begin
  Status('1 < 2 and (3 < 4 or 5 < 6)');

  with TNodeFactory do
    LExpected := AndBExp(
      RelOpBExp('<', IntegerAExp(1), IntegerAExp(2)),
      OrBExp(
        RelOpBExp('<', IntegerAExp(3), IntegerAExp(4)),
        RelOpBExp('<', IntegerAExp(5), IntegerAExp(6))));

//  (procedure(const AVisitor: TCombinatorVisitor)
//  begin
//    try
//      BExp.Accept(AVisitor);
//    finally
//      AVisitor.Free;
//    end;
//  end)(TCombinatorVisitor.Create(Status));

  ParserTest('1 < 2 and (3 < 4 or 5 < 6)', BExp, LExpected);

//  TConfig.OnBefore := nil;
end;

procedure TTestParser.TestBExpNot;
var
  LAst: INode;
begin
  with TNodeFactory do
    LAst :=
      NotBExp(
        RelOpBExp('<',
          IntegerAExp(2),
          IntegerAExp(3)));
  ParserTest('not 2 < 3', BExp, LAst);
end;

procedure TTestParser.TestBExpNotPrecedence;
var
  LExpected: INode;
begin
  with TNodeFactory do
    LExpected := AndBExp(
      NotBExp(RelOpBExp('<', IntegerAExp(1), IntegerAExp(2))),
      RelOpBExp('<', IntegerAExp(3), IntegerAExp(4)));
  ParserTest('not 1 < 2 and 3 < 4', BExp, LExpected);
end;

procedure TTestParser.TestBExpRelationOperator;
var
  LAst: INode;
begin
  with TNodeFactory do
    LAst := RelOpBExp('<',
      IntegerAExp(2),
      IntegerAExp(3));
  ParserTest('2 < 3', BExp, LAst);
end;

procedure TTestParser.TestCompoundStatement;
var
  LAst: INode;
begin
  with TNodeFactory do
    LAst := CompoundStatement(
      AssignStatement('x',
        IntegerAExp(1)),
      AssignStatement('y',
        IntegerAExp(2)));
  ParserTest('x := 1; y := 2', Statements_List, LAst);
end;

procedure TTestParser.TestIfStatement;
var
  LExpected: INode;
begin
  with TNodeFactory do
    LExpected := IfStatement(
      RelOpBExp('<', IntegerAExp(1), IntegerAExp(2)),
      AssignStatement('x', IntegerAExp(3)),
      AssignStatement('x', IntegerAExp(4)));
  ParserTest('if 1 < 2 then x := 3 else x := 4 end', Statements_List, LExpected);

  with TNodeFactory do
    LExpected := IfStatement(
      RelOpBExp('<', IntegerAExp(1), IntegerAExp(2)),
      AssignStatement('x', IntegerAExp(3)));
  ParserTest('if 1 < 2 then x := 3 end', Statements_List, LExpected);
end;

procedure TTestParser.TestPrecedence;

  function Combine: TFunc<TValue,TValue>;
  begin
    Result :=
      function(AValue: TValue): TValue
      var
        LOpFunction: TFunc<Integer,Integer,Integer>;
      begin
        if AValue.AsString = '*' then
          LOpFunction := function(ALeft, ARight: Integer): Integer begin Result := ALeft * ARight end
        else
          LOpFunction := function(ALeft, ARight: Integer): Integer begin Result := ALeft + ARight end;

        Result := TValue.From<TFunc<TValue,TValue,TValue>>(
          function(ALeft, ARight: TValue): TValue
          begin
            try
              Result := LOpFunction(ALeft.AsInteger, ARight.AsInteger);
            finally
              LOpFunction := nil;
            end;
          end)
      end;
  end;

var
  LLevels: TPrecedenceLevels;
  LParser: TParser;
begin
  LLevels := TPrecedenceLevels.Create(
    TPrecedenceLevel.Create('*'),
    TPrecedenceLevel.Create('+'));

  LParser := Precedence(Num, LLevels, Combine());
  ParserTest('2 * 3 + 4', LParser, 10);
  ParserTest('2 + 3 * 4', LParser, 14);
end;

procedure TTestParser.TestWhileStatement;
var
  LExpected: INode;
begin
  with TNodeFactory do
    LExpected := WhileStatement(
      RelOpBExp('<', IntegerAExp(1), IntegerAExp(2)),
      AssignStatement('x', IntegerAExp(3)));

  ParserTest('while 1 < 2 do x := 3 end', Statements_List, LExpected);
end;

initialization
  RegisterTest(TTestParser.Suite);

end.
