unit Test_uCombinators.IInput;
{

  Delphi DUnit Test Case
  ----------------------
  This unit contains a skeleton test case class generated by the Test Case Wizard.
  Modify the generated code to correctly setup and call the methods from the unit 
  being tested.

}

interface

uses
  TestFramework,
  System.Rtti,
  uCombinators.IInput;

type
  // Test methods for class IInput

  TestIInput = class(TTestCase)
  strict private
    FInput: IInput;
    class function IntToTValue(AValue: Integer): TValue; static;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  published
    procedure TestAdvance;
    procedure TestGetSource;
    procedure TestGetCurrent;
    procedure TestGetAtEnd;
    procedure TestGetPosition;
    procedure TestGetLine;
    procedure TestGetColumn;
    procedure TestGetMemos;
    procedure TestToString;
  end;

implementation

uses
  System.SysUtils,
  System.Generics.Collections,
  System.Generics.Defaults,
  uArrayHelper;

class function TestIInput.IntToTValue(AValue: Integer): TValue;
begin
  Result := AValue;
end;

procedure TestIInput.SetUp;
begin
  FInput := TInput.Build(TArray<TValue>.Create(10, 20, 30, 40));
end;

procedure TestIInput.TearDown;
begin
  FInput := nil;
end;

procedure TestIInput.TestAdvance;
begin
  CheckEquals(10, FInput.Current.AsInteger, '#1');
  CheckNotEquals(0, FInput.Current.AsInteger, '#2');
  CheckEquals(20, FInput.Advance.Current.AsInteger, '#3');
  CheckEquals(30, FInput.Advance.Advance.Current.AsInteger, '#4');
  CheckEquals(40, FInput.Advance.Advance.Advance.Current.AsInteger, '#5');
  CheckTrue(FInput.Advance.Advance.Advance.Advance.AtEnd, '#6');
end;

procedure TestIInput.TestGetSource;
begin
  Check(
    TEqualityComparer<TArray<TValue>>.Default.Equals(
      TArray<TValue>.Create(10, 20, 30, 40), FInput.Source), '#1');

  Check(
    TEqualityComparer<TArray<TValue>>.Default.Equals(
      TArray<TValue>.Create(10, 20, 30, 40), FInput.Advance.Source), '#2');
end;

procedure TestIInput.TestGetCurrent;
begin
  CheckEquals(10, FInput.Current.AsInteger, '#1');
  CheckNotEquals(0, FInput.Current.AsInteger, '#2');
  CheckEquals(20, FInput.Advance.Current.AsInteger, '#3');
end;

procedure TestIInput.TestGetAtEnd;
begin
  CheckFalse(FInput.AtEnd, '#1');
  CheckTrue(FInput.Advance.Advance.Advance.Advance.AtEnd, '#2');
end;

procedure TestIInput.TestGetPosition;
begin
  CheckEquals(0, FInput.Position, '#1');
  CheckEquals(1, FInput.Advance.Position, '#2');
end;

procedure TestIInput.TestGetLine;
begin
  CheckEquals(1, FInput.Line, '#1');
  CheckEquals(1, FInput.Advance.Line, '#2');
end;

procedure TestIInput.TestGetColumn;
begin
  CheckEquals(1, FInput.Column, '#1');
  CheckEquals(1, FInput.Advance.Column, '#2');
end;

procedure TestIInput.TestGetMemos;
begin
  CheckEquals(0, FInput.Memos.Count, '#1');
  CheckEquals(0, FInput.Advance.Memos.Count, '#2');
end;

procedure TestIInput.TestToString;
begin
  CheckEquals('Line 1, Column 1', FInput.ToString, '#1');
  CheckEquals('Line 1, Column 1', FInput.Advance.ToString, '#2');
end;

initialization
  // Register any test cases with the test runner
  RegisterTest(TestIInput.Suite);
end.

