program Tests;
{

  Delphi DUnit Test Project
  -------------------------
  This project contains the DUnit test framework and the GUI/Console test runners.
  Add "CONSOLE_TESTRUNNER" to the conditional defines entry in the project options
  to use the console test runner.  Otherwise the GUI test runner will be used by
  default.

}

{$IFDEF CONSOLE_TESTRUNNER}
{$APPTYPE CONSOLE}
{$ENDIF}

uses
  DUnitTestRunner,
  Test_uLexer in 'Test_uLexer.pas',
  uLexer in '..\src\uLexer.pas',
  Test_uCombinators in 'Test_uCombinators.pas',
  uCombinators in '..\src\uCombinators.pas',
  uImp_Lexer in '..\src\uImp_Lexer.pas',
  uValues in '..\src\uValues.pas',
  uCombinators.Interfaces in '..\src\uCombinators.Interfaces.pas',
  uImp_Combinators in '..\src\uImp_Combinators.pas',
  uAst.Interfaces in '..\src\uAst.Interfaces.pas',
  uAst in '..\src\uAst.pas',
  uImp_Ast in '..\src\uImp_Ast.pas',
  uImp_Parser in '..\src\uImp_Parser.pas',
  Test_uParser in 'Test_uParser.pas',
  uGenericsComparer in '..\src\uGenericsComparer.pas',
  uImp.Parser.Values in '..\src\uImp.Parser.Values.pas',
  Test_uImp_Ast.ExecutionEngine in 'Test_uImp_Ast.ExecutionEngine.pas',
  uImp_Ast.ExecutionEngine in '..\src\uImp_Ast.ExecutionEngine.pas',
  uCombinators.IInput in '..\src\uCombinators.IInput.pas',
  uCombinators.Input in '..\src\uCombinators.Input.pas',
  uCombinators.IResult in '..\src\uCombinators.IResult.pas',
  uCombinators.Result in '..\src\uCombinators.Result.pas',
  Test_uCombinators.IInput in 'Test_uCombinators.IInput.pas',
  uArrayHelper in '..\src\uArrayHelper.pas',
  Test_uArrayHelper in 'Test_uArrayHelper.pas',
  Test_uCombinators.IResult in 'Test_uCombinators.IResult.pas';

{$R *.RES}

begin
  ReportMemoryLeaksOnShutdown := True;
  DUnitTestRunner.RunRegisteredTests;
end.

