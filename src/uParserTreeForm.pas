unit uParserTreeForm;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls,
  uCombinators.Interfaces;

type
  TParserTreeForm = class(TForm, ICombinatorVisitor)
    tvCombinatorTree: TTreeView;
  private
    FCombinator: ICombinator;
    FCurrentNode: TTreeNode;
    procedure SetCombinator(const Value: ICombinator);

    function AddNode(const AParent: TTreeNode; const ACaption: string): TTreeNode;
    procedure AddCombinator(const AInfo: string; const ACombinator: ICombinator; const AChilds: array of ICombinator);
  protected
    procedure Visit(const ACombinator: IConcatenate); overload;
    procedure Visit(const ACombinator: IExp); overload;
    procedure Visit(const ACombinator: IAlternate); overload;
    procedure Visit(const ACombinator: IProcess); overload;
    procedure Visit(const ACombinator: IReserved); overload;
    procedure Visit(const ACombinator: ITag); overload;
    procedure Visit(const ACombinator: IOption); overload;
    procedure Visit(const ACombinator: IRepeat); overload;
    procedure Visit(const ACombinator: ILazy); overload;
    procedure Visit(const ACombinator: IPhrase); overload;
  public
    property Combinator: ICombinator read FCombinator write SetCombinator;
  end;

implementation

{$R *.dfm}

{ TParserTreeForm }

procedure TParserTreeForm.AddCombinator(const AInfo: string;
  const ACombinator: ICombinator; const AChilds: array of ICombinator);
var
  LCombinator: ICombinator;
begin
  FCurrentNode := AddNode(FCurrentNode, Format('%s %s',[AInfo, ACombinator.Description]));
  try
    for LCombinator in AChilds do
      LCombinator.Accept(Self);
  finally
    FCurrentNode := FCurrentNode.Parent;
  end;
end;

function TParserTreeForm.AddNode(const AParent: TTreeNode; const ACaption: string): TTreeNode;
begin
  with tvCombinatorTree.Items do
    Result := AddChild(AParent, ACaption);
end;

procedure TParserTreeForm.SetCombinator(const Value: ICombinator);
begin
  FCombinator := Value;
  tvCombinatorTree.Items.BeginUpdate;
  try
    tvCombinatorTree.Items.Clear;
    FCurrentNode := AddNode(tvCombinatorTree.TopItem, '[root]');
    FCombinator.Accept(Self);
  finally
    tvCombinatorTree.Items.EndUpdate;
  end;
  tvCombinatorTree.FullExpand;
end;

procedure TParserTreeForm.Visit(const ACombinator: IProcess);
begin
  AddCombinator('Process: ', ACombinator, [ACombinator.Parser]);
end;

procedure TParserTreeForm.Visit(const ACombinator: IReserved);
begin
  AddCombinator(Format('Reserved: Value: "%s"; Tag: "%s";',
    [ACombinator.Value.ToString, ACombinator.Tag.ToString]), ACombinator, []);
end;

procedure TParserTreeForm.Visit(const ACombinator: IAlternate);
begin
  AddCombinator('Alternate: ', ACombinator, [ACombinator.Left, ACombinator.Right]);
end;

procedure TParserTreeForm.Visit(const ACombinator: IConcatenate);
begin
  AddCombinator('Concatenate: ', ACombinator, [ACombinator.Left, ACombinator.Right]);
end;

procedure TParserTreeForm.Visit(const ACombinator: IExp);
begin
  AddCombinator('Exp: ', ACombinator, [ACombinator.Parser, ACombinator.Separator]);
end;

procedure TParserTreeForm.Visit(const ACombinator: ILazy);
begin
  AddCombinator('Lazy: ', ACombinator, []);
end;

procedure TParserTreeForm.Visit(const ACombinator: IPhrase);
begin
  AddCombinator('Phrase: ', ACombinator, [ACombinator.Parser]);
end;

procedure TParserTreeForm.Visit(const ACombinator: IRepeat);
begin
  AddCombinator('Repeat: ', ACombinator, [ACombinator.Parser]);
end;

procedure TParserTreeForm.Visit(const ACombinator: ITag);
begin
  AddCombinator(Format('Tag: "%s"', [ACombinator.Tag.ToString]), ACombinator, []);
end;

procedure TParserTreeForm.Visit(const ACombinator: IOption);
begin
  AddCombinator('Option: ', ACombinator, [ACombinator.Parser]);
end;

end.
