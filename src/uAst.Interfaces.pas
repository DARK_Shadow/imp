unit uAst.Interfaces;

interface

uses
  System.Rtti;

type
  IRepresentable = interface
//  ['{B9942564-68F5-47D0-A923-AC6D350A9B52}']
    function ToString: string;
  end;

  IEnvironment = interface
//  ['{9505DC7C-D0AA-4314-9159-D3CB5DB9648F}']
    procedure SetValue(const AName: string; const AValue: TValue);
    function GetValue(const AName: string): TValue;
    function Contains(const AName: string): Boolean;
    procedure Clean;
    property Value[const AName: string]: TValue read GetValue write SetValue; default;
  end;

  INodeVisitor = interface;

  INode = interface(IRepresentable)
//  ['{4B85C31C-7164-43A4-ACFD-BF50ACC17EBD}']
    procedure Accept(const AVisitor: INodeVisitor);
    function GetGUID: TGUID;

    property GUID: TGUID read GetGUID;
  end;

  IStatement = interface(INode)
//  ['{F2E3C2F2-4FC0-484D-A525-A8984683F70B}']
  end;

  IExpression = interface(INode)
//  ['{CE2E6133-5B3D-4DC1-8B09-51DAE26AEA40}']
  end;

  IAExp = interface(IExpression)
//  ['{C2054E22-2F78-4AB0-824E-875F6F2BBF12}']
  end;

  IBExp = interface(IExpression)
//  ['{10679874-A7C7-4E28-91D9-2449EFA431DD}']
  end;

  IAssignStatement = interface(IStatement)
//  ['{57F8A85B-ED69-4776-84A7-75AB3221C9F7}']
    function GetName: string;
    function GetExpression: IExpression;

    property Name: string read GetName;
    property Expression: IExpression read GetExpression;
  end;

  ICompoundStatement = interface(IStatement)
//  ['{B491A187-04F4-4CAD-8CDC-34371B2AA76E}']
    function GetFirst: IStatement;
    function GetSecond: IStatement;

    property First: IStatement read GetFirst;
    property Second: IStatement read GetSecond;
  end;

  IIfStatement = interface(IStatement)
//  ['{BC5DF7A9-F6AC-4D4C-915E-CA46FCE04AAC}']
    function GetCondition: IExpression;
    function GetTrueStatement: IStatement;
    function GetFalseStatement: IStatement;

    property Condition: IExpression read GetCondition;
    property TrueStatement: IStatement read GetTrueStatement;
    property FalseStatemtnt: IStatement read GetFalseStatement;
  end;

  IWhileStatement = interface(IStatement)
//  ['{AF1F723F-FB2F-4603-BC61-C02C090484F0}']
    function GetCondition: IExpression;
    function GetBody: IStatement;

    property Condition: IExpression read GetCondition;
    property Body: IStatement read GetBody;
  end;

  IValueExpression<TValueType> = interface(IAExp)
//  ['{6B4C0B1B-1EBF-45B3-B8CD-CACE7B4C7105}']
    function GetValue: TValueType;

    property Value: TValueType read GetValue;
  end;

  IIntegerExpression = interface(IValueExpression<Integer>)
//  ['{25ED7984-7612-4868-AFCB-896D233A760D}']
  end;

  IVariableExpression = interface(IExpression)
//  ['{41660EEF-CEB1-43FB-9BEA-362E6C6A8037}']
    function GetName: string;

    property Name: string read GetName;
  end;

  IUnaryExpression = interface(IExpression)
//  ['{579E479A-C82A-4AA4-AFC3-10F3BDB82185}']
    function GetOperator: TValue;
    function GetExpression: IExpression;

    property Operator: TValue read GetOperator;
    property Expression: IExpression read GetExpression;
  end;

  IBinaryExpression = interface(IExpression)
//  ['{A491FC93-2229-4181-BD4F-70CC7691C4C9}']
    function GetOperator: TValue;
    function GetLeft: IExpression;
    function GetRight: IExpression;

    property Operator: TValue read GetOperator;
    property Left: IExpression read GetLeft;
    property Right: IExpression read GetRight;
  end;

  INodeVisitor = interface
//  ['{C443471B-038C-4DAC-BFAC-A91D15BF11F4}']
    procedure Visit(const ANode: IAssignStatement); overload;
    procedure Visit(const ANode: ICompoundStatement); overload;
    procedure Visit(const ANode: IIfStatement); overload;
    procedure Visit(const ANode: IWhileStatement); overload;
    procedure Visit(const ANode: IIntegerExpression); overload;
    procedure Visit(const ANode: IVariableExpression); overload;
    procedure Visit(const ANode: IBinaryExpression); overload;
    procedure Visit(const ANode: IUnaryExpression); overload;
  end;

implementation

end.
