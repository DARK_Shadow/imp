unit uCombinators;

interface

uses
  uCombinators.Interfaces,
  System.SysUtils,
  System.Rtti,
  uCombinators.IInput,
  uCombinators.IResult;

type
  TCombinedValue = record
    Left: TValue;
    Right: TValue;
    constructor Create(const ALeft, ARight: TValue);
    class operator Implicit(const AValue: TCombinedValue): TValue;
    class operator Explicit(const AValue: TCombinedValue): TValue;
    class operator Implicit(const AValue: TValue): TCombinedValue;
    class operator Explicit(const AValue: TValue): TCombinedValue;
  end;

  TFactory = class(TCombinatorFactory)
  public
    class function CreateReserved(const AValue, ATag: TValue; const AConfigurator: TCombinatorConfigClass): IReserved; override;
    class function CreateTag(const ATag: TValue; const AConfigurator: TCombinatorConfigClass): ITag; override;
    class function CreateOption(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass): IOption; override;
    class function CreateRepeat(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass): IRepeat; override;
    class function CreateLazy(AParserConstructor: TFunc<TParserFunc>; const AConfigurator: TCombinatorConfigClass): ILazy; override;
    class function CreatePhrase(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass): IPhrase; override;
    class function CreateAlternate(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass): IAlternate; override;
    class function CreateConcatenate(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass): IConcatenate; override;
    class function CreateProcess(const AParser: ICombinator; AFunction: TFunc<TValue,TValue>; const AConfigurator: TCombinatorConfigClass): IProcess; override;
    class function CreateExpression(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass): IExp; override;
  end;

implementation

uses
  System.Generics.Defaults,
  System.Generics.Collections,
  uCombinators.Result;

type
  TCombinator = class abstract(TSingletonImplementation, ICombinator, TParserFunc)
  strict private
    FConfigurator: TCombinatorConfigClass;
    FDescription: string;
  strict protected
    function Invoke(AInput: IInput): IResult; virtual; abstract;
    procedure Accept(const AVisitor: ICombinatorVisitor); virtual; abstract;

    constructor Create(const AConfigurator: TCombinatorConfigClass); virtual;
    property Configurator: TCombinatorConfigClass read FConfigurator;
  protected
    function Concatenate(const AOther: ICombinator): ICombinator; virtual;
    function Expression(const AOther: ICombinator): ICombinator; virtual;
    function Alternate(const AOther: ICombinator): ICombinator; virtual;
    function Process(AFunction: TFunc<TValue,TValue>): ICombinator; virtual;

    function GetDescription: string;
    procedure SetDescription(const ADescription: string);
  end;

  TUnaryCombinator = class(TCombinator, IUnaryCombinator)
  strict private
    FParser: ICombinator;
    function GetParser: ICombinator;
  strict protected
    property Parser: ICombinator read GetParser;
  public
    constructor Create(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass); reintroduce; virtual;
  end;

  TBinaryCombinator = class(TCombinator, IBinaryCombinator)
  strict private
    FLeft: ICombinator;
    FRight: ICombinator;
    function GetLeft: ICombinator;
    function GetRight: ICombinator;
  strict protected
    property Left: ICombinator read GetLeft;
    property Right: ICombinator read GetRight;
  public
    constructor Create(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass); reintroduce; virtual;
  end;

  TReserved = class(TCombinator, IReserved)
  strict private
    FValue: TValue;
    FTag: TValue;
  private
    function GetTag: TValue;
    function GetValue: TValue;
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;
    property Value: TValue read GetValue;
    property Tag: TValue read GetTag;
  public
    constructor Create(const AValue: TValue; const ATag: TValue; const AConfigurator: TCombinatorConfigClass); reintroduce; virtual;
  end;

  TTag = class(TCombinator, ITag)
  strict private
    FTag: TValue;
  private
    function GetTag: TValue;
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;
    property Tag: TValue read GetTag;
  public
    constructor Create(const ATag: TValue; const AConfigurator: TCombinatorConfigClass); reintroduce; virtual;
  end;

  TConcatenate = class(TBinaryCombinator, IConcatenate)
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;
  public
    property Right;
    property Left;
  end;

  TExpression = class(TBinaryCombinator, IExp)
  private
    function GetParser: ICombinator;
    function GetSeparator: ICombinator;
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;
  public
    property Right;
    property Left;
  end;

  TAlternate = class(TBinaryCombinator, IAlternate)
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;
  public
    property Right;
    property Left;
  end;

  TOption = class(TUnaryCombinator, IOption)
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;
  public
    property Parser;
  end;

  TRepeat = class(TUnaryCombinator, IRepeat)
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;
  public
    property Parser;
  end;

  TProcess = class(TUnaryCombinator, IProcess)
  strict private
    FFunction: TFunc<TValue,TValue>;
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;

    property &Function: TFunc<TValue,TValue> read FFunction;
  public
    constructor Create(const AParser: ICombinator; AFunction: TFunc<TValue,TValue>; const AConfigurator: TCombinatorConfigClass); reintroduce; virtual;

    property Parser;
  end;

  TLazy = class(TCombinator, ILazy)
  strict private
    FParser: TParserFunc;
    FParserConstructor: TFunc<TParserFunc>;
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;
  public
    constructor Create(AParserConstructor: TFunc<TParserFunc>; const AConfigurator: TCombinatorConfigClass); reintroduce; virtual;

    property Parser: TParserFunc read FParser write FParser;
    property ParserConstructor: TFunc<TParserFunc> read FParserConstructor;
  end;

  TPhrase = class(TUnaryCombinator, IPhrase)
  strict protected
    procedure Accept(const AVisitor: ICombinatorVisitor); override;
    function Invoke(AInput: IInput): IResult; override;
  public
    property Parser;
  end;

{ TCombinator }

function TCombinator.Alternate(const AOther: ICombinator): ICombinator;
begin
  Result := Configurator.GetFactory.CreateAlternate(Self, AOther, Configurator);
end;

function TCombinator.Concatenate(const AOther: ICombinator): ICombinator;
begin
  Result := Configurator.GetFactory.CreateConcatenate(Self, AOther, Configurator);
end;

constructor TCombinator.Create(const AConfigurator: TCombinatorConfigClass);
begin
  inherited Create;
  FConfigurator := AConfigurator;
  FConfigurator.AddToCache(Self);
  FDescription := '';
end;

function TCombinator.Expression(const AOther: ICombinator): ICombinator;
begin
  Result := Configurator.GetFactory.CreateExpression(Self, AOther, Configurator);
end;

function TCombinator.GetDescription: string;
begin
  Result := FDescription;
end;

function TCombinator.Process(AFunction: TFunc<TValue, TValue>): ICombinator;
begin
  Result := Configurator.GetFactory.CreateProcess(Self, AFunction, Configurator);
end;

procedure TCombinator.SetDescription(const ADescription: string);
begin
  FDescription := ADescription;
end;

{ TReserved }

procedure TReserved.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TReserved.Create(const AValue, ATag: TValue; const AConfigurator: TCombinatorConfigClass);
begin
  inherited Create(AConfigurator);
  FValue := AValue;
  FTag := ATag;
end;

function TReserved.GetTag: TValue;
begin
  Result := FTag;
end;

function TReserved.GetValue: TValue;
begin
  Result := FValue;
end;

function TReserved.Invoke(AInput: IInput): IResult;
var
  LCurrentToken: TValue;
  LTokenText: string;
  LTokenTag: TValue;
begin
  if not AInput.AtEnd then
  begin
    LCurrentToken := AInput.Current;
    LTokenText := Configurator.GetTokenPropertyGetter.GetTokenText(LCurrentToken);
    LTokenTag := Configurator.GetTokenPropertyGetter.GetTokenTag(LCurrentToken);

    if SameText(Value.AsString, LTokenText) and SameText(Tag.ToString, LTokenTag.ToString) then
      Exit(TResult.Success(LTokenText, AInput.Advance));

    Exit(TResult.Failure(AInput, Format('unexpected "%s as %s"', [LTokenText, LTokenTag.ToString]),
      TArray<string>.Create('TReserved(Description)')));
  end;
  Result := TResult.Failure(AInput, 'Unexpected end of input reached',
    TArray<string>.Create('TReserved(Description)'));
end;

{ TBaseCombinator }

constructor TBinaryCombinator.Create(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass);
begin
  Inherited Create(AConfigurator);
  FLeft := ALeft;
  FRight := ARight;
end;

{ TTag }

procedure TTag.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TTag.Create(const ATag: TValue; const AConfigurator: TCombinatorConfigClass);
begin
  inherited Create(AConfigurator);
  FTag := ATag;
end;

function TTag.GetTag: TValue;
begin
  Result := FTag;
end;

function TTag.Invoke(AInput: IInput): IResult;
var
  LCurrentToken: TValue;
  LTokenTag: TValue;
  LTokenText: string;
begin
  if not AInput.AtEnd then
  begin
    LCurrentToken := AInput.Current;
    LTokenText := Configurator.GetTokenPropertyGetter.GetTokenText(LCurrentToken);
    LTokenTag := Configurator.GetTokenPropertyGetter.GetTokenTag(LCurrentToken);

    if SameText(Tag.ToString, LTokenTag.ToString) then
      Exit(TResult.Success(LTokenText, AInput.Advance));

    Exit(TResult.Failure(AInput, Format('unexpected "%s"', [LTokenTag.ToString]),
      TArray<string>.Create('TTag(Description)')));
  end;
  Result := TResult.Failure(AInput, 'Unexpected end of input reached',
    TArray<string>.Create('TReserved(Description)'));
end;

{ TConcatenate }

procedure TConcatenate.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

function TConcatenate.Invoke(AInput: IInput): IResult;
var
  LLeftResult: IResult;
  LRightResult: IResult;
begin
  LLeftResult := TParserFunc(Self.Left)(AInput);
  if LLeftResult.WasSuccessful then
  begin
    LRightResult := TParserFunc(Self.Right)(LLeftResult.Remainder);
    if LRightResult.WasSuccessful then
      Exit(TResult.Success(
        TCombinedValue.Create(LLeftResult.Value, LRightResult.Value), LRightResult.Remainder));

    Exit(TResult.Failure(LLeftResult.Remainder, LRightResult.Message, LRightResult.Expectations));
  end;
  Result := TResult.Failure(AInput, LLeftResult.Message, LLeftResult.Expectations);
end;

{ TCombinedValue }

constructor TCombinedValue.Create(const ALeft, ARight: TValue);
begin
  Left := ALeft;
  Right := ARight;
end;

class operator TCombinedValue.Explicit(const AValue: TCombinedValue): TValue;
begin
  Result := TValue.From<TCombinedValue>(AValue);
end;

class operator TCombinedValue.Implicit(const AValue: TCombinedValue): TValue;
begin
  Result := TValue.From<TCombinedValue>(AValue);
end;

class operator TCombinedValue.Explicit(const AValue: TValue): TCombinedValue;
begin
  Result := AValue.AsType<TCombinedValue>;
end;

class operator TCombinedValue.Implicit(const AValue: TValue): TCombinedValue;
begin
  Result := AValue.AsType<TCombinedValue>;
end;

{ TExpression }

procedure TExpression.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

function TExpression.GetParser: ICombinator;
begin
  Result := Left;
end;

function TExpression.GetSeparator: ICombinator;
begin
  Result := Right;
end;

//  result = self.parser(tokens, pos)
//
//  def process_next(parsed):
//      (sepfunc, right) = parsed
//      return sepfunc(result.value, right)
//  next_parser = self.separator + self.parser ^ process_next
//
//  next_result = result
//  while next_result:
//      next_result = next_parser(tokens, result.pos)
//      if next_result:
//          result = next_result
//  return result
function TExpression.Invoke(AInput: IInput): IResult;
var
  LNextParser: ICombinator;
  LNextResult: IResult;
  LResult: IResult;
begin
  LResult := TParserFunc(Self.Left)(AInput);

  LNextParser := Self.Right.Concatenate(Self.Left).Process(
    function(AValue: TValue): TValue
    var
      LCombined: TCombinedValue;
    begin
      LCombined := AValue;
      Result := LCombined.Left.AsType<TFunc<TValue,TValue,TValue>>
        .Invoke(LResult.Value, LCombined.Right);
    end);

  LNextResult := LResult;
  while LNextResult.WasSuccessful do
  begin
    LNextResult := LNextParser.Invoke(LResult.Remainder);
    if LNextResult.WasSuccessful then
      LResult := LNextResult;
  end;
  Result := LResult;
end;

{ TAlternate }

//def __call__(self, tokens, pos):
//    left_result = self.left(tokens, pos)
//    if left_result:
//        return left_result
//    else:
//        right_result = self.right(tokens, pos)
//        return right_result
procedure TAlternate.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

function TAlternate.Invoke(AInput: IInput): IResult;
var
  LLeftResult: IResult;
begin
  LLeftResult := TParserFunc(Self.Left)(AInput);
  if LLeftResult.WasSuccessful then
    Result := LLeftResult
  else
    Result := TParserFunc(Self.Right)(AInput);
end;

{ TOption }

//  result = self.parser(tokens, pos)
//  if result:
//      return result
//  else:
//      return Result(None, pos)
procedure TOption.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

function TOption.Invoke(AInput: IInput): IResult;
begin
  Result := Parser.Invoke(AInput);
  if not Result.WasSuccessful then
    Result := TResult.Success(TValue(nil), AInput);
end;

{ TRepeat }

//  results = []
//  result = self.parser(tokens, pos)
//  while result:
//      results.append(result.value)
//      pos = result.pos
//      result = self.parser(tokens, pos)
//  return Result(results, pos)
procedure TRepeat.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

function TRepeat.Invoke(AInput: IInput): IResult;
var
  LResults: TList<TValue>;
begin
  LResults := TList<TValue>.Create;
  try
    Result := Parser.Invoke(AInput);
    while Result.WasSuccessful do
    begin
      LResults.Add(Result.Value);
      Result := Parser.Invoke(Result.Remainder);
    end;
    Result := TResult.Success(TValue.From<TArray<TValue>>(LResults.ToArray), Result.Remainder);
  finally
    LResults.Free;
  end;
end;

{ TProcess }

procedure TProcess.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TProcess.Create(const AParser: ICombinator; AFunction: TFunc<TValue, TValue>; const AConfigurator: TCombinatorConfigClass);
begin
  inherited Create(AParser, AConfigurator);
  FFunction := AFunction;
end;

//  result = self.parser(tokens, pos)
//  if result:
//      result.value = self.function(result.value)
//      return result
function TProcess.Invoke(AInput: IInput): IResult;
begin
  Result := Parser.Invoke(AInput);
  if Result.WasSuccessful then
    Result := TResult.Success(&Function(Result.Value), Result.Remainder);
end;

{ TUnaryCombinator }

constructor TUnaryCombinator.Create(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass);
begin
  inherited Create(AConfigurator);
  FParser := AParser;
end;

{ TLazy }

procedure TLazy.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TLazy.Create(AParserConstructor: TFunc<TParserFunc>; const AConfigurator: TCombinatorConfigClass);
begin
  inherited Create(AConfigurator);
  FParser := nil;
  FParserConstructor := AParserConstructor;
end;

//  if not self.parser:
//      self.parser = self.parser_func()
//  return self.parser(tokens, pos)
function TLazy.Invoke(AInput: IInput): IResult;
begin
  if not Assigned(Parser) then
    Parser := ParserConstructor();
  Result := Parser(AInput);
end;

{ TPhrase }

//  result = self.parser(tokens, pos)
//  if result and result.pos == len(tokens):
//      return result
//  else:
//      return None
procedure TPhrase.Accept(const AVisitor: ICombinatorVisitor);
begin
  AVisitor.Visit(Self);
end;

function TPhrase.Invoke(AInput: IInput): IResult;
begin
  Result := Parser.Invoke(AInput);
  if Result.WasSuccessful and Result.Remainder.AtEnd then
  else
    Result := TResult.Failure(AInput, Result.Message, Result.Expectations);
end;

function TUnaryCombinator.GetParser: ICombinator;
begin
  Result := FParser;
end;

{ TBinaryCombinator }

function TBinaryCombinator.GetLeft: ICombinator;
begin
  Result := FLeft;
end;

function TBinaryCombinator.GetRight: ICombinator;
begin
  Result := FRight;
end;

{ TFactory }

class function TFactory.CreateRepeat(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass): IRepeat;
begin
  Result := TRepeat.Create(AParser, AConfigurator);
end;

class function TFactory.CreateReserved(const AValue, ATag: TValue;
  const AConfigurator: TCombinatorConfigClass): IReserved;
begin
  Result := TReserved.Create(AValue, ATag, AConfigurator);
end;

class function TFactory.CreateTag(const ATag: TValue; const AConfigurator: TCombinatorConfigClass): ITag;
begin
  Result := TTag.Create(ATag, AConfigurator);
end;

class function TFactory.CreateAlternate(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass): IAlternate;
begin
  Result := TAlternate.Create(ALeft, ARight, AConfigurator);
end;

class function TFactory.CreateConcatenate(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass): IConcatenate;
begin
  Result := TConcatenate.Create(ALeft, ARight, AConfigurator);
end;

class function TFactory.CreateExpression(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass): IExp;
begin
  Result := TExpression.Create(ALeft, ARight, AConfigurator);
end;

class function TFactory.CreateLazy(AParserConstructor: TFunc<TParserFunc>;
  const AConfigurator: TCombinatorConfigClass): ILazy;
begin
  Result := TLazy.Create(AParserConstructor, AConfigurator);
end;

class function TFactory.CreateOption(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass): IOption;
begin
  Result := TOption.Create(AParser, AConfigurator);
end;

class function TFactory.CreatePhrase(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass): IPhrase;
begin
  Result := TPhrase.Create(AParser, AConfigurator);
end;

class function TFactory.CreateProcess(const AParser: ICombinator; AFunction: TFunc<TValue,TValue>; const AConfigurator: TCombinatorConfigClass): IProcess;
begin
  Result := TProcess.Create(AParser, AFunction, AConfigurator);
end;

end.
