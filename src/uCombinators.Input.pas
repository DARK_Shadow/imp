unit uCombinators.Input;

interface

uses
  uCombinators.IInput,
  System.Generics.Collections,
  System.Rtti;

type
  TInput = class(TInterfacedObject, IInput)
  private
    FSource: TArray<TValue>;
    FPosition: Integer;
    FLine: Integer;
    FColumn: Integer;
    FMemos: TDictionary<TObject,TObject>;
  protected
    function Advance: IInput;
    function GetAtEnd: Boolean;
    function GetColumn: Integer;
    function GetCurrent: TValue;
    function GetLine: Integer;
    function GetMemos: TDictionary<TObject,TObject>;
    function GetPosition: Integer;
    function GetSource: System.TArray<System.Rtti.TValue>;

    constructor Create(const ASource: TArray<TValue>; const APosition: Integer; const ALine: Integer = 1; const AColumn: Integer = 1); overload;
  public
    constructor Create(const ASource: TArray<TValue>); overload;
    destructor Destroy; override;
    function ToString: string; override;
    function Equals(Obj: TObject): Boolean; overload; override;
    function Equals(Obj: IInput): Boolean; reintroduce; overload;
    function GetHashCode: Integer; override;
  end;

implementation

uses
  System.SysUtils,
  System.Generics.Defaults;

{ TInput }

function TInput.Advance: IInput;
begin
  if GetAtEnd then
    raise EInvalidOpException.Create('The input is already at the end of the source.');

  Result := TInput.Create(FSource, FPosition + 1);
end;

constructor TInput.Create(const ASource: TArray<TValue>; const APosition, ALine, AColumn: Integer);
begin
  inherited Create;
  FSource := ASource;
  FPosition := APosition;
  FLine := ALine;
  FColumn := AColumn;

  FMemos := TObjectDictionary<TObject,TObject>.Create;
end;

constructor TInput.Create(const ASource: TArray<TValue>);
begin
  Create(ASource, Low(ASource));
end;

destructor TInput.Destroy;
begin
  FMemos.Free;
  inherited;
end;

function TInput.Equals(Obj: TObject): Boolean;
begin
  Result := Equals(IInput(Obj as TInput));
end;

function TInput.Equals(Obj: IInput): Boolean;
begin
  if not Assigned(Obj) then
    Exit(False);

  if IInput(Self) = Obj then
    Exit(True);

  Result := (Self.FSource = Obj.Source) and (FPosition = Obj.Position);
end;

function TInput.GetAtEnd: Boolean;
begin
  Result := FPosition = Length(FSource);
end;

function TInput.GetColumn: Integer;
begin
  Result := FColumn;
end;

function TInput.GetCurrent: TValue;
begin
  Result := FSource[FPosition];
end;

function TInput.GetHashCode: Integer;
var
  LSourceHashCode: Integer;
begin
  if Assigned(FSource) then
    LSourceHashCode := TEqualityComparer<TArray<TValue>>.Default.GetHashCode(FSource)
  else
    LSourceHashCode := 0;

  Result := (LSourceHashCode * 397) xor FPosition;
end;

function TInput.GetLine: Integer;
begin
  Result := FLine;
end;

function TInput.GetMemos: TDictionary<System.TObject, System.TObject>;
begin
  Result := FMemos;
end;

function TInput.GetPosition: Integer;
begin
  Result := FPosition;
end;

function TInput.GetSource: System.TArray<System.Rtti.TValue>;
begin
  Result := FSource;
end;

function TInput.ToString: string;
begin
  Result := Format('Line %d, Column %d', [FLine, FColumn]);
end;

end.
