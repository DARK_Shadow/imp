unit uAst;

interface

uses
  System.Rtti,
  System.Generics.Collections,
  uAst.Interfaces;

type
  TNode = class abstract(TInterfacedObject)
  strict private
    FGUID: TGUID;
  protected
    procedure Accept(const AVisitor: INodeVisitor); virtual; abstract;
    function GetGUID: TGUID;
    constructor Create;
    property GUID: TGUID read GetGUID;
  public
    function ToString: string; override; abstract;
  end;

  TStatement = class abstract(TNode, IRepresentable, IStatement, INode);

  TExpression = class abstract(TNode, IRepresentable, IExpression, INode);

  TAExpression = class abstract(TExpression, IAExp);

  TBExpression = class abstract(TExpression, IBExp);

  TAssignStatement = class(TStatement, IAssignStatement, INode)
  private
    FName: string;
    FAExpression: IExpression;
  protected
    function GetName: string;
    function GetExpression: IExpression;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const AName: string; const AExpression: IExpression);
  end;

  TCompoundStatement = class(TStatement, ICompoundStatement)
  private
    FFirst: IStatement;
    FSecond: IStatement;
  protected
    function GetFirst: IStatement;
    function GetSecond: IStatement;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const AFirst, ASecond: IStatement);
  end;

  TIfStatement = class(TStatement, IIfStatement)
  private
    FCondition: IExpression;
    FTrueStatement: IStatement;
    FFalseStatement: IStatement;
  protected
    function GetCondition: IExpression;
    function GetTrueStatement: IStatement;
    function GetFalseStatement: IStatement;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const ACondition: IExpression; const ATrueStatement: IStatement;
      const AFalseStatement: IStatement = nil);
  end;

  TWhileStatement = class(TStatement, IWhileStatement)
  private
    FCondition: IExpression;
    FBody: IStatement;
  protected
    function GetCondition: IExpression;
    function GetBody: IStatement;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const ACondition: IExpression; const ABody: IStatement);
  end;

  TIntegerAExp = class(TAExpression, IIntegerExpression)
  private
    FValue: Integer;
  protected
    function GetValue: Integer;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const AValue: Integer);
  end;

  TVariableAExp = class(TAExpression, IVariableExpression)
  private
    FName: string;
  protected
    function GetName: string;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const AName: string);
  end;

  TBinaryAExp = class(TAExpression, IBinaryExpression)
  private
    FOperator: TValue;
    FLeftExpression: IExpression;
    FRightExpression: IExpression;
  protected
    function GetOperator: TValue;
    function GetLeft: IExpression;
    function GetRight: IExpression;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const AOperator: TValue; const ALeftExpression, ARightExpression: IExpression);
  end;

  TRelationBExp = class(TBExpression, IBinaryExpression)
  private
    FOperator: TValue;
    FLeftExpression: IExpression;
    FRightExpression: IExpression;
  protected
    function GetOperator: TValue;
    function GetLeft: IExpression;
    function GetRight: IExpression;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const AOperator: TValue; const ALeftExpression, ARightExpression: IExpression);
  end;

  TAndBExp = class(TBExpression, IBinaryExpression)
  private
    FLeft: IExpression;
    FRight: IExpression;
  protected
    function GetOperator: TValue;
    function GetLeft: IExpression;
    function GetRight: IExpression;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const ALeftExpression, ARightExpression: IExpression);
  end;

  TOrBExp = class(TBExpression, IBinaryExpression)
  private
    FLeft: IExpression;
    FRight: IExpression;
  protected
    function GetOperator: TValue;
    function GetLeft: IExpression;
    function GetRight: IExpression;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const ALeftExpression, ARightExpression: IExpression);
  end;

  TNotBExp = class(TBExpression, IUnaryExpression)
  private
    FExpression: IExpression;
  protected
    function GetOperator: TValue;
    function GetExpression: IExpression;

    procedure Accept(const AVisitor: INodeVisitor); override;
  public
    function ToString: string; override;
    constructor Create(const AExpression: IExpression);
  end;

  TEnvironment = class(TInterfacedObject, IEnvironment)
  private
    FDictionary: TDictionary<string,TValue>;
  protected
    procedure SetValue(const AName: string; const AValue: TValue);
    function GetValue(const AName: string): TValue;
    function Contains(const AName: string): Boolean;
    procedure Clean;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  System.SysUtils;

{ TAssignStatement<TValue> }

procedure TAssignStatement.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TAssignStatement.Create(const AName: string; const AExpression: IExpression);
begin
  inherited Create;
  FName := AName;
  FAExpression := AExpression;
end;

function TAssignStatement.GetExpression: IExpression;
begin
  Result := FAExpression;
end;

function TAssignStatement.GetName: string;
begin
  Result := FName;
end;

function TAssignStatement.ToString: string;
begin
  Result :=  Format('AssignStatement(%s, %s)', [FName, FAExpression.ToString])
end;

{ TCompoundStatement<TValue> }

procedure TCompoundStatement.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TCompoundStatement.Create(const AFirst, ASecond: IStatement);
begin
  inherited Create;
  FFirst := AFirst;
  FSecond := ASecond;
end;

function TCompoundStatement.GetFirst: IStatement;
begin
  Result := FFirst;
end;

function TCompoundStatement.GetSecond: IStatement;
begin
  Result := FSecond;
end;

function TCompoundStatement.ToString: string;
begin
  Result := Format('CompoundStatement(%s, %s)', [FFirst.ToString, FSecond.ToString])
end;

{ TIfStatement<TValue> }

procedure TIfStatement.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TIfStatement.Create(const ACondition: IExpression; const ATrueStatement, AFalseStatement: IStatement);
begin
  inherited Create;
  FCondition := ACondition;
  FTrueStatement := ATrueStatement;
  FFalseStatement := AFalseStatement;
end;

function TIfStatement.GetCondition: IExpression;
begin
  Result := FCondition;
end;

function TIfStatement.GetFalseStatement: IStatement;
begin
  Result := FFalseStatement;
end;

function TIfStatement.GetTrueStatement: IStatement;
begin
  Result := FTrueStatement;
end;

function TIfStatement.ToString: string;
begin
  if Assigned(FFalseStatement) then
    Result := Format('IfStatement(%s, %s, %s)', [FCondition.ToString, FTrueStatement.ToString, FFalseStatement.ToString])
  else
    Result := Format('IfStatement(%s, %s)', [FCondition.ToString, FTrueStatement.ToString]);
end;

{ TWhileStatement<TValue> }

procedure TWhileStatement.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TWhileStatement.Create(const ACondition: IExpression; const ABody: IStatement);
begin
  inherited Create;
  FCondition := ACondition;
  FBody := ABody;
end;

function TWhileStatement.GetBody: IStatement;
begin
  Result := FBody;
end;

function TWhileStatement.GetCondition: IExpression;
begin
  Result := FCondition;
end;

function TWhileStatement.ToString: string;
begin
  Result := Format('WhileStatement(%s, %s)', [Fcondition.ToString, FBody.ToString])
end;

{ TIntegerAExp<TValue> }

procedure TIntegerAExp.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TIntegerAExp.Create(const AValue: Integer);
begin
  inherited Create;
  FValue := AValue;
end;

function TIntegerAExp.GetValue: Integer;
begin
  Result := FValue;
end;

function TIntegerAExp.ToString: string;
begin
  Result := Format('IntAexp(%d)', [FValue]);
//  Result := Format('%d', [FValue]);
end;

{ TVariableAExp<TValue> }

procedure TVariableAExp.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TVariableAExp.Create(const AName: string);
begin
  inherited Create;
  FName := AName;
end;

function TVariableAExp.GetName: string;
begin
  Result := FName;
end;

function TVariableAExp.ToString: string;
begin
  Result := Format('VarAexp(%s)', [FName]);
//  Result := Format('%s', [FName]);
end;

{ TBinaryAExp }

procedure TBinaryAExp.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TBinaryAExp.Create(const AOperator: TValue;
  const ALeftExpression, ARightExpression: IExpression);
begin
  FOperator := AOperator;
  FLeftExpression := ALeftExpression;
  FRightExpression := ARightExpression;
end;

function TBinaryAExp.GetLeft: IExpression;
begin
  Result := FLeftExpression;
end;

function TBinaryAExp.GetOperator: TValue;
begin
  Result := FOperator;
end;

function TBinaryAExp.GetRight: IExpression;
begin
  Result := FRightExpression;
end;

function TBinaryAExp.ToString: string;
begin
  Result := Format('BinaryAExp(%s, %s, %s)', [{TConverter.AsString(FOperator)}'@',
    FLeftExpression.ToString, FRightExpression.ToString]);

//  Result := Format('(%s %s %s)', [FLeftExpression.ToString, {TConverter.AsString(FOperator)}'@', FRightExpression.ToString]);
end;

{ TRelationBExp }

procedure TRelationBExp.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TRelationBExp.Create(const AOperator: TValue; const ALeftExpression, ARightExpression: IExpression);
begin
  inherited Create;
  FOperator := AOperator;
  FLeftExpression := ALeftExpression;
  FRightExpression := ARightExpression;
end;

function TRelationBExp.GetLeft: IExpression;
begin
  Result := FLeftExpression;
end;

function TRelationBExp.GetOperator: TValue;
begin
  Result := FOperator;
end;

function TRelationBExp.GetRight: IExpression;
begin
  Result := FRightExpression;
end;

function TRelationBExp.ToString: string;
begin
  Result := Format('RelopBexp(%s, %s, %s)', [{TConverter.AsString(FOperator)}'@',
    FLeftExpression.ToString, FRightExpression.ToString]);
//  Result := Format('(%s %s %s)', [FLeftExpression.ToString, {TConverter.AsString(FOperator)}'@', FRightExpression.ToString]);
end;

{ TAndBExp }

procedure TAndBExp.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TAndBExp.Create(const ALeftExpression, ARightExpression: IExpression);
begin
  inherited Create;
  FLeft := ALeftExpression;
  FRight := ARightExpression;
end;

function TAndBExp.GetLeft: IExpression;
begin
  Result := FLeft;
end;

function TAndBExp.GetOperator: TValue;
begin
  raise ENotImplemented.Create('TAndBExp.GetOperator');
//  Result := TConverter.CreateOperator('and');
end;

function TAndBExp.GetRight: IExpression;
begin
  Result := FRight;
end;

function TAndBExp.ToString: string;
begin
  Result := Format('AndBexp(%s, %s)', [FLeft.ToString, FRight.ToString]);
//  Result := Format('(%s and %s)', [FLeft.ToString, FRight.ToString]);
end;

{ TOrBExp }

procedure TOrBExp.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TOrBExp.Create(const ALeftExpression, ARightExpression: IExpression);
begin
  inherited Create;
  FLeft := ALeftExpression;
  FRight := ARightExpression;
end;

function TOrBExp.GetLeft: IExpression;
begin
  Result := FLeft;
end;

function TOrBExp.GetOperator: TValue;
begin
  raise ENotImplemented.Create('TOrBExp.GetOperator');
//  Result := TConverter.CreateOperator('or');
end;

function TOrBExp.GetRight: IExpression;
begin
  Result := FRight;
end;

function TOrBExp.ToString: string;
begin
  Result := Format('OrBexp(%s, %s)', [FLeft.ToString, FRight.ToString]);
//  Result := Format('(%s or %s)', [FLeft.ToString, FRight.ToString]);
end;

{ TNotBExp }

procedure TNotBExp.Accept(const AVisitor: INodeVisitor);
begin
  AVisitor.Visit(Self);
end;

constructor TNotBExp.Create(const AExpression: IExpression);
begin
  inherited Create;
  FExpression := AExpression;
end;

function TNotBExp.GetExpression: IExpression;
begin
  Result := FExpression;
end;

function TNotBExp.GetOperator: TValue;
begin
  raise ENotImplemented.Create('TNotBExp.GetOperator');
//  Result := TConverter.CreateOperator('not');
end;

function TNotBExp.ToString: string;
begin
  Result := Format('NotBexp(%s)', [FExpression.ToString]);
//  Result := Format('(not %s)', [FExpression.ToString]);
end;

{ TEnviroment }

procedure TEnvironment.Clean;
begin
  FDictionary.Clear;
end;

function TEnvironment.Contains(const AName: string): Boolean;
begin
  Result := FDictionary.ContainsKey(AName);
end;

constructor TEnvironment.Create;
begin
  inherited Create;
  FDictionary := TDictionary<string,TValue>.Create;
end;

destructor TEnvironment.Destroy;
begin
  FDictionary.Free;
  inherited;
end;

function TEnvironment.GetValue(const AName: string): TValue;
begin
  Result := FDictionary[AName];
end;

procedure TEnvironment.SetValue(const AName: string;
  const AValue: TValue);
begin
  FDictionary.AddOrSetValue(AName, AValue);
end;

{ TNode }

constructor TNode.Create;
begin
  inherited;
  FGUID := TGUID.Empty;
end;

function TNode.GetGUID: TGUID;
begin
  if FGUID = TGUID.Empty then
    FGUID := TGUID.NewGuid;
  Result := FGUID;
end;

end.
