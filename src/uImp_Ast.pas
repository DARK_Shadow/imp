unit uImp_Ast;

interface

uses
  uAst.Interfaces,
  System.Rtti,
  uValues,
  System.SysUtils;

type
  TNodeFactory = class abstract
  public
    class function AssignStatement(const AName: string; const AExpression: IExpression): IStatement; static;
    class function CompoundStatement(const AFirst, ASecond: IStatement): IStatement; static;
    class function IfStatement(const ACondition: IExpression; const ATrueStatement: IStatement;
      const AFalseStatement: IStatement = nil): IStatement; static;
    class function WhileStatement(const ACondition: IExpression; const ABody: IStatement): IStatement; static;

    class function IntegerAExp(const AValue: Integer): IExpression; static;
    class function VariableAExp(const AName: string): IExpression; static;
    class function BinOpAExp(const AOperator: string; const ALeft, ARight: IExpression): IExpression; static;

    class function RelOpBExp(const AOperator: string; const ALeft, ARight: IExpression): IExpression; static;
    class function AndBExp(const ALeft, ARight: IExpression): IExpression; static;
    class function OrBExp(const ALeft, ARight: IExpression): IExpression; static;
    class function NotBExp(const AExpression: IExpression): IExpression; static;
  end;

implementation

uses
  uAst;

{ TNodeFactory }

class function TNodeFactory.AssignStatement(const AName: string; const AExpression: IExpression): IStatement;
begin
  Result := TAssignStatement.Create(AName, AExpression);
end;

class function TNodeFactory.CompoundStatement(const AFirst, ASecond: IStatement): IStatement;
begin
  Result := TCompoundStatement.Create(AFirst, ASecond);
end;

class function TNodeFactory.IfStatement(const ACondition: IExpression; const ATrueStatement: IStatement;
  const AFalseStatement: IStatement = nil): IStatement;
begin
  Result := TIfStatement.Create(ACondition, ATrueStatement, AFalseStatement);
end;

class function TNodeFactory.WhileStatement(const ACondition: IExpression; const ABody: IStatement): IStatement;
begin
  Result := TWhileStatement.Create(ACondition, ABody);
end;

class function TNodeFactory.IntegerAExp(const AValue: Integer): IExpression;
begin
  Result := TIntegerAExp.Create(AValue);
end;

class function TNodeFactory.VariableAExp(const AName: string): IExpression;
begin
  Result := TVariableAExp.Create(AName);
end;

class function TNodeFactory.BinOpAExp(const AOperator: string; const ALeft, ARight: IExpression): IExpression;
begin
  Result := TBinaryAExp.Create(AOperator, ALeft, ARight);
end;

class function TNodeFactory.RelOpBExp(const AOperator: string; const ALeft, ARight: IExpression): IExpression;
begin
  Result := TRelationBExp.Create(AOperator, ALeft, ARight);
end;

class function TNodeFactory.AndBExp(const ALeft, ARight: IExpression): IExpression;
begin
  Result := TAndBExp.Create(ALeft, ARight);
end;

class function TNodeFactory.OrBExp(const ALeft, ARight: IExpression): IExpression;
begin
  Result := TOrBExp.Create(ALeft, ARight);
end;

class function TNodeFactory.NotBExp(const AExpression: IExpression): IExpression;
begin
  Result := TNotBExp.Create(AExpression);
end;

end.
