unit uImp_Parser;

interface

uses
  System.SysUtils,
  System.Rtti,
  uImp_Combinators;

type
  TPrecedenceLevel = array of string;
  TPrecedenceLevels = array of TPrecedenceLevel;

  TPrecedenceLevelHelper = record helper for TPrecedenceLevel
  end;

function Keyword(const AKeyword: string): TParser;
function Num: TParser;
function Id: TParser;
//function Imp_Parse(const ATokens: array of TToken): IStatement;
function Parser: TParser;

function Statements_List: TParser;
function Statements: TParser;

function Assign_Statement: TParser;
function If_Statement: TParser;
function While_Statement: TParser;

function BExp: TParser;

function AExp: TParser;

function Precedence(const AValueParser: TParser; const APrecedenceLevels: array of TPrecedenceLevel; ACombine: TFunc<TValue,TValue>): TParser;

implementation

uses
  System.Generics.Collections,
  uImp_Lexer,
  uAst.Interfaces,
  uImp_Ast,
  uCombinators,
  uCombinators.Interfaces;

const
  AExpPrecedenceLevels: array[0..1] of array[0..1] of string = (
    ('*', '/'),
    ('+', '-'));

  BExpPrecedenceLevels: array[0..1] of array[0..0] of string = (
    ('and'),
    ('or'));

function ProcessGroup: TFunc<TValue,TValue>; forward;
function BExpTerm: TParser; forward;
function AnyOperatorInList(const AOperators: array of string): TParser; forward;

function Keyword(const AKeyword: string): TParser;
begin
  Result := Reserved(AKeyword, ttReserved) + Format('Keyword("%s")', [AKeyword]);
end;

function Num: TParser;
begin
  Result := (Tag(ttInt) xor
    (function(AValue: TValue): TValue
    begin
      Result := AValue.AsString.ToInteger;
    end)) + 'Number(String->Integer)';
end;

function Id: TParser;
begin
  Result := Tag(ttId) + 'Identeficator';
end;

//function Imp_Parse(const ATokens: array of TToken): IStatement;
//begin
//end;

function Parser: TParser;
begin
  Result := Phrase(Statements_List) + 'Root_Parser';
end;


//  separator = keyword(';') ^ (lambda x: lambda l, r: CompoundStatement(l, r))
//  return Exp(stmt(), separator)

function Statements_List: TParser;
var
  LSeparator: TParser;
begin
  LSeparator := (Keyword(';') xor
    (function(AValue: TValue): TValue
    begin
      Result := TValue.From<TFunc<TValue,TValue,TValue>>(
        function(ALeft, ARight: TValue): TValue
        begin
          Result := TValue.From<IStatement>(
            TNodeFactory.CompoundStatement(ALeft.AsType<IStatement>, ARight.AsType<IStatement>));
        end)
    end)) + ' -> ICompoundStatement';
  Result := Exp(Statements, LSeparator) + 'Statement_List';
end;

function Statements: TParser;
begin
  Result := Assign_Statement or
    If_Statement or
    While_Statement + 'Statements';
end;

//  def process(parsed):
//      ((name, _), exp) = parsed
//      return AssignStatement(name, exp)
//  return id + keyword(':=') + aexp() ^ process

function Assign_Statement: TParser;
begin
  Result := (Id + Keyword(':=') + AExp xor
    (function(AValue: TValue): TValue
    var
      LName: string;
      LExpression: IExpression;
    begin
      with TCombinedValue(AValue) do
      begin
        LExpression := Right.AsType<IExpression>;
        with TCombinedValue(Left) do
          LName := Left.AsString;
      end;
      Result := TValue.From<IStatement>(
        TNodeFactory.AssignStatement(LName, LExpression));
    end)) + 'Assign_Statement -> IAssignStatement';
end;

//def if_stmt():
//    def process(parsed):
//        (((((_, condition), _), true_stmt), false_parsed), _) = parsed
//        if false_parsed:
//            (_, false_stmt) = false_parsed
//        else:
//            false_stmt = None
//        return IfStatement(condition, true_stmt, false_stmt)
//    return keyword('if') + bexp() + \
//           keyword('then') + Lazy(stmt_list) + \
//           Opt(keyword('else') + Lazy(stmt_list)) + \
//           keyword('end') ^ process
function If_Statement: TParser;
var
  LStatements_List: TFunc<TParserFunc>;
begin
  LStatements_List :=
    function: TParserFunc
    begin
      Result := Statements_List();
    end;

  Result := (Keyword('if') + BExp
    + Keyword('then') + Lazy(LStatements_List)
    + Opt(Keyword('else') + Lazy(LStatements_List))
    + Keyword('end') xor
      (function(AValue: TValue): TValue
      var
        LFalse: TValue;
        LTrue: TValue;
        LCondition: TValue;
      begin
        with TCombinedValue(AValue) do
          with TCombinedValue(Left) do
          begin
            LFalse := Right;
            with TCombinedValue(Left) do
            begin
              LTrue := Right;
              with TCombinedValue(Left) do
                with TCombinedValue(Left) do
                  LCondition := Right;
            end;
            if not LFalse.IsEmpty then
              LFalse := TCombinedValue(LFalse).Right
            else
              LFalse := TValue.From<IStatement>(nil);
          end;

          Result := TValue.From<IStatement>(
            TNodeFactory.IfStatement(LCondition.AsType<IExpression>, LTrue.AsType<IStatement>, LFalse.AsType<IStatement>));
      end)) + 'If_Statement -> IIfStatement';
end;

//def while_stmt():
//    def process(parsed):
//        ((((_, condition), _), body), _) = parsed
//        return WhileStatement(condition, body)
//    return keyword('while') + bexp() + \
//           keyword('do') + Lazy(stmt_list) + \
//           keyword('end') ^ process
function While_Statement: TParser;
var
  LStatements_List: TFunc<TParserFunc>;
begin
  LStatements_List :=
    function: TParserFunc
    begin
      Result := Statements_List();
    end;

  Result :=
    (Keyword('while') + BExp +
    Keyword('do') + Lazy(LStatements_List) +
    Keyword('end') xor
      (function(AValue: TValue): TValue
      var
        LCondition: TValue;
        LBody: TValue;
      begin
        with TCombinedValue(AValue) do
          with TCombinedValue(Left) do
          begin
            LBody := Right;
            with TCombinedValue(Left) do
              with TCombinedValue(Left) do
                LCondition := Right;
          end;
        Result := TValue.From<IStatement>(
          TNodeFactory.WhileStatement(LCondition.AsType<IExpression>, LBody.AsType<IStatement>));
      end)) + 'While_Statement -> IWhileStatement';
end;

//def process_relop(parsed):
//    ((left, op), right) = parsed
//    return RelopBexp(op, left, right)
function ProcessRelationOperation: TFunc<TValue,TValue>;
begin
  Result :=
    function(AValue: TValue): TValue
    var
      LLeft, LRight: TValue;
      LOperator: string;
    begin
      with TCombinedValue(AValue) do
      begin
        with TCombinedValue(Left) do
        begin
          LLeft := Left;
          LOperator := Right.AsString;
        end;
        LRight := Right;
      end;
      Result := TValue.From<IExpression>(
        TNodeFactory.RelOpBExp(LOperator, LLeft.AsType<IExpression>, LRight.AsType<IExpression>));
    end;
end;

//def process_logic(op):
//    if op == 'and':
//        return lambda l, r: AndBexp(l, r)
//    elif op == 'or':
//        return lambda l, r: OrBexp(l, r)
//    else:
//        raise RuntimeError('unknown logic operator: ' + op)
function ProcessLogic: TFunc<TValue,TValue>;
begin
  Result :=
    function(AOperator: TValue): TValue
    var
      LOperator: string;
    begin
      LOperator := AOperator.AsString;
      if SameText(LOperator, 'and') then
      begin
        Result := TValue.From<TFunc<TValue,TValue,TValue>>(
          function(ALeft, ARight: TValue): TValue
          begin
            Result := TValue.From<IExpression>(
              TNodeFactory.AndBExp(ALeft.AsType<IExpression>, ARight.AsType<IExpression>))
          end)
      end
      else
      if SameText(LOperator, 'or') then
      begin
        Result := TValue.From<TFunc<TValue,TValue,TValue>>(
          function(ALeft, ARight: TValue): TValue
          begin
            Result := TValue.From<IExpression>(
              TNodeFactory.OrBExp(ALeft.AsType<IExpression>, ARight.AsType<IExpression>))
          end)
      end
      else
        raise Exception.Create('Unknown logic operator: ' + LOperator);
    end;
end;

//def bexp_not():
//    return keyword('not') + Lazy(bexp_term) ^ (lambda parsed: NotBexp(parsed[1]))
function BExpNot: TParser;
var
  LBExpTerm: TFunc<TParserFunc>;
begin
  LBExpTerm :=
    function: TParserFunc
    begin
      Result := BExpTerm();
    end;

  Result := (Keyword('not') + Lazy(LBExpTerm) xor
    (function(AParsed: TValue): TValue
    begin
      Result := TValue.From<IExpression>(
        TNodeFactory.NotBExp(TCombinedValue(AParsed).Right.AsType<IExpression>));
    end)) + 'Not_Expression -> IUnaryExpression("not")';
end;

//def bexp_relop():
//    relops = ['<', '<=', '>', '>=', '=', '!=']
//    return aexp() + any_operator_in_list(relops) + aexp() ^ process_relop
function BExpRelationOp: TParser;
begin
  Result := (AExp + AnyOperatorInList(['<', '<=', '>', '>=', '=', '!=']) +
    AExp xor (ProcessRelationOperation)) + 'Relation_Operator_Expression -> IBinaryExpression("<", "<=", ">", ">=", "=", "!=")';
end;

//def bexp_group():
//    return keyword('(') + Lazy(bexp) + keyword(')') ^ process_group
function BExpGroup: TParser;
var
  LBExp: TFunc<TParserFunc>;
begin
  LBExp :=
    function: TParserFunc
    begin
      Result := BExp();
    end;

  Result := (Keyword('(') + Lazy(LBExp) + Keyword(')') xor (ProcessGroup)) + 'Logic_Group_Expression';
end;

//def bexp():
//    return precedence(bexp_term(),
//                      bexp_precedence_levels,
//                      process_logic)
function BExp: TParser;
var
  LPrecedenceLevels: TPrecedenceLevels;
begin
  LPrecedenceLevels := TPrecedenceLevels.Create(
    TPrecedenceLevel.Create('and'),
    TPrecedenceLevel.Create('or'));
  Result := Precedence(BExpTerm, LPrecedenceLevels, ProcessLogic()) + 'Logic_Expression -> IBinaryExpression("and", "or")';
end;

//def bexp_term():
//    return bexp_not()   | \
//           bexp_relop() | \
//           bexp_group()
function BExpTerm: TParser;
begin
  Result := (BExpNot
    or BExpRelationOp
    or BExpGroup) + 'Logic_Expressions';
end;

//  ((_, p), _) = parsed
//  return p
function ProcessGroup: TFunc<TValue,TValue>;
begin
  Result :=
    function(AValue: TValue): TValue
    begin
      with TCombinedValue(AValue) do
        with TCombinedValue(Left) do
          Result := Right;
    end;
end;

function AExpGroup: TParser;
var
  LAExpLazy: TFunc<TParserFunc>;
begin
  LAExpLazy :=
    function: TParserFunc
    begin
      Result := AExp();
    end;

  Result := ((Keyword('(') + Lazy(LAExpLazy) + Keyword(')')) xor ProcessGroup()) + 'Arithmetic_Group_Expression';
end;

function AExpValue: TParser;
begin
  Result := (((Num xor
    (function(AValue: TValue): TValue
    begin
      Result := TValue.From<IExpression>(TNodeFactory.IntegerAExp(AValue.AsInteger));
    end)) + ' -> IIntegerExpression')
    or ((Id xor
    (function(AValue: TValue): TValue
    begin
      Result := TValue.From<IExpression>(TNodeFactory.VariableAExp(AValue.AsString));
    end)) + ' -> IVariableExpression')) + 'Arithmetic_Value_Expression';
end;

function AExpTerm: TParser;
begin
  Result := (AExpValue or AExpGroup) + 'Arithmetic_Expressions';
end;

//  return lambda l, r: BinopAexp(op, l, r)

function ProcessBinOp: TFunc<TValue,TValue>;
begin
  Result :=
    function(AOperator: TValue): TValue
    begin
      Result := TValue.From<TFunc<TValue,TValue,TValue>>(
        function(ALeft, ARight: TValue): TValue
        begin
          Result := TValue.From<IExpression>(
            TNodeFactory.BinOpAExp(AOperator.AsString, ALeft.AsType<IExpression>, ARight.AsType<IExpression>));
        end)
    end;
end;

function AExp: TParser;
var
  LPrecedenceLevels: TPrecedenceLevels;
begin
  LPrecedenceLevels := TPrecedenceLevels.Create(
    TPrecedenceLevel.Create('*', '/'),
    TPrecedenceLevel.Create('+', '-'));
  Result := Precedence(AExpTerm, LPrecedenceLevels, ProcessBinOp()) + 'Arithmetic_Expression -> IBinaryExpression([["*", "/"],["+", "-"]])';
end;

function AnyOperatorInList(const AOperators: array of string): TParser;
var
  LOpParsers: TList<TParser>;
  LOperator: string;
  LIndex: Integer;
begin
  LOpParsers := TList<TParser>.Create;
  try
    for LOperator in AOperators do
      LOpParsers.Add(Keyword(LOperator));

    Result := LOpParsers[0];
    for LIndex := 1 to LOpParsers.Count - 1 do
     Result := Result or LOpParsers[LIndex]
  finally
    LOpParsers.Free;
  end;
  Result := Result + 'Operator_List'
end;

function Precedence(const AValueParser: TParser; const APrecedenceLevels: array of TPrecedenceLevel;
  ACombine: TFunc<TValue,TValue>): TParser;

  function OperatorParser(const APrecedenceLevel: array of string): TParser;
  begin
    Result := AnyOperatorInList(APrecedenceLevel) xor
      (function(AValue: TValue): TValue
      begin
        Result := ACombine(AValue);
      end)
  end;

var
  LIndex: Integer;
begin
  Result := AValueParser * OperatorParser(APrecedenceLevels[Low(APrecedenceLevels)]);
  for LIndex := Succ(Low(APrecedenceLevels)) to High(APrecedenceLevels) do
    Result := Result * OperatorParser(APrecedenceLevels[LIndex]);

  Result := Result + 'Operator_Precedence';
end;

end.
