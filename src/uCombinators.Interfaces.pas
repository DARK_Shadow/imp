unit uCombinators.Interfaces;

interface

uses
  System.Rtti,
  System.SysUtils,
  uCombinators.IInput,
  uCombinators.IResult;

type
  TParserFunc = reference to function(AInput: IInput): IResult;

  ICombinatorVisitor = interface;

  IParser = interface
    function Invoke(AInput: IInput): IResult;
    procedure Accept(const AVisitor: ICombinatorVisitor);
  end;

  ICombinator = interface(IParser)
    function Concatenate(const AOther: ICombinator): ICombinator;
    function Expression(const AOther: ICombinator): ICombinator;
    function Alternate(const AOther: ICombinator): ICombinator;
    function Process(AFunction: TFunc<TValue,TValue>): ICombinator;

    function GetDescription: string;
    procedure SetDescription(const ADescription: string);

    property Description: string read GetDescription write SetDescription;
  end;

  IUnaryCombinator = interface(ICombinator)
    function GetParser: ICombinator;
    property Parser: ICombinator read GetParser;
  end;

  IBinaryCombinator = interface(ICombinator)
    function GetLeft: ICombinator;
    function GetRight: ICombinator;
    property Left: ICombinator read GetLeft;
    property Right: ICombinator read GetRight;
  end;

  IConcatenate = interface(IBinaryCombinator)
  end;

  IExp = interface(ICombinator)
    function GetParser: ICombinator;
    function GetSeparator: ICombinator;

    property Parser: ICombinator read GetParser;
    property Separator: ICombinator read GetSeparator;
  end;

  IAlternate = interface(IBinaryCombinator)
  end;

  IProcess = interface(IUnaryCombinator)
  end;

  IReserved = interface(ICombinator)
    function GetValue: TValue;
    function GetTag: TValue;

    property Value: TValue read GetValue;
    property Tag: TValue read GetTag;
  end;

  ITag = interface(ICombinator)
    function GetTag: TValue;

    property Tag: TValue read GetTag;
  end;

  IOption = interface(IUnaryCombinator)
  end;

  IRepeat = interface(IUnaryCombinator)
  end;

  ILazy = interface(ICombinator)
  end;

  IPhrase = interface(IUnaryCombinator)
  end;

  ICombinatorVisitor = interface
    procedure Visit(const ACombinator: IConcatenate); overload;
    procedure Visit(const ACombinator: IExp); overload;
    procedure Visit(const ACombinator: IAlternate); overload;
    procedure Visit(const ACombinator: IProcess); overload;
    procedure Visit(const ACombinator: IReserved); overload;
    procedure Visit(const ACombinator: ITag); overload;
    procedure Visit(const ACombinator: IOption); overload;
    procedure Visit(const ACombinator: IRepeat); overload;
    procedure Visit(const ACombinator: ILazy); overload;
    procedure Visit(const ACombinator: IPhrase); overload;
  end;

  TCombinatorConfig = class;
  TCombinatorConfigClass = class of TCombinatorConfig;

  TCombinatorFactory = class abstract
  public
    class function CreateReserved(const AValue, ATag: TValue; const AConfigurator: TCombinatorConfigClass): IReserved; virtual; abstract;
    class function CreateTag(const ATag: TValue; const AConfigurator: TCombinatorConfigClass): ITag; virtual; abstract;
    class function CreateOption(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass): IOption; virtual; abstract;
    class function CreateRepeat(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass): IRepeat; virtual; abstract;
    class function CreateLazy(AParserConstructor: TFunc<TParserFunc>; const AConfigurator: TCombinatorConfigClass): ILazy; virtual; abstract;
    class function CreatePhrase(const AParser: ICombinator; const AConfigurator: TCombinatorConfigClass): IPhrase; virtual; abstract;
    class function CreateConcatenate(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass): IConcatenate; virtual; abstract;
    class function CreateExpression(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass): IExp; virtual; abstract;
    class function CreateAlternate(const ALeft, ARight: ICombinator; const AConfigurator: TCombinatorConfigClass): IAlternate; virtual; abstract;
    class function CreateProcess(const AParser: ICombinator; AFunction: TFunc<TValue,TValue>; const AConfigurator: TCombinatorConfigClass): IProcess; virtual; abstract;
  end;

  TCombinatorFactoryClass = class of TCombinatorFactory;

  TTokenPropertyGetter = class abstract
  public
    class function GetTokenText(const AToken: TValue): string; virtual; abstract;
    class function GetTokenTag(const AToken: TValue): TValue; virtual; abstract;
  end;

  TTokenPropertyGetterClass = class of TTokenPropertyGetter;

  TCombinatorsCache = class abstract
  public
    procedure Add(const ACombinator: TObject); virtual; abstract;
    procedure Clean; virtual; abstract;

    constructor Create; virtual; abstract;
  end;

  TCombinatorsCacheClass = class of TCombinatorsCache;

  TCombinatorConfig = class abstract
  private class var
    FCombinatorsCache: TCombinatorsCache;
  public
    class function GetFactory: TCombinatorFactoryClass; virtual; abstract;
    class function GetTokenPropertyGetter: TTokenPropertyGetterClass; virtual; abstract;
    class function GetCombinatorsCache: TCombinatorsCacheClass; virtual; abstract;

    class procedure AddToCache(const ACombinator: TObject); virtual;
    class procedure CleanCache; virtual;

    class destructor Destroy;
  end;

implementation

{ TCombinatorConfig }

class procedure TCombinatorConfig.AddToCache(const ACombinator: TObject);
begin
  if not Assigned(FCombinatorsCache) then
    FCombinatorsCache := GetCombinatorsCache.Create;

  FCombinatorsCache.Add(ACombinator);
end;

class procedure TCombinatorConfig.CleanCache;
begin
  if not Assigned(FCombinatorsCache) then
    FCombinatorsCache := GetCombinatorsCache.Create;

  FCombinatorsCache.Clean;
end;

class destructor TCombinatorConfig.Destroy;
begin
  if Assigned(FCombinatorsCache) then
    FreeAndNil(FCombinatorsCache);
end;

end.
