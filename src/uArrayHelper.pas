unit uArrayHelper;

interface

uses
  System.SysUtils,
  System.Generics.Collections;

type
  TArrayHelper = class helper for TArray
  public
    class function Aggregate<T,TOut>(const AInput: TArray<T>; Converter: TFunc<T,TOut>; Aggregator: TFunc<TOut, TOut, TOut>): TOut; static;
    class function Select<TSource,TResult>(const ASource: TArray<TSource>; ASelector: TFunc<TSource,TResult>): TArray<TResult>; static;
  end;

  TPredicateOf<T> = class abstract
  strict private
    class var FAlways: TPredicate<T>;
    class constructor Create;
  public
    class property Always: TPredicate<T> read FAlways;
  end;

  TFunctionOf<T> = class abstract
  strict private
    class var FIdentity: TFunc<T,T>;
    class constructor Create;
  public
    class property Identity: TFunc<T,T> read FIdentity;
  end;

implementation

{ TArrayHelper }

class function TArrayHelper.Aggregate<T, TOut>(const AInput: TArray<T>; Converter: TFunc<T,TOut>; Aggregator: TFunc<TOut, TOut, TOut>): TOut;
var
  LIndex: Integer;
begin
  if Length(AInput) = 0 then
    Exit(Default(TOut));
  Result := Converter(AInput[Low(AInput)]);
  for LIndex := Succ(Low(AInput)) to High(AInput) do
    Result := Aggregator(Result, Converter(AInput[LIndex]));
end;

class function TArrayHelper.Select<TSource, TResult>(const ASource: TArray<TSource>;
  ASelector: TFunc<TSource, TResult>): TArray<TResult>;
var
  LIndex: Integer;
begin
  SetLength(Result, Length(ASource));
  for LIndex := Low(ASource) to High(ASource) do
    Result[LIndex] := ASelector(ASource[LIndex]);
end;

{ TArrayHelper.TPredicateOf<T> }

class constructor TPredicateOf<T>.Create;
begin
  FAlways :=
    function(AValue: T): Boolean
    begin
      Result := True;
    end;
end;

{ TFunctionOf<T> }

class constructor TFunctionOf<T>.Create;
begin
  FIdentity :=
    function(AValue: T): T
    begin
      Result := AValue;
    end;
end;

end.
