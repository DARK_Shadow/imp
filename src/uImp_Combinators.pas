unit uImp_Combinators;

interface

uses
  uCombinators.Interfaces,
  uCombinators.IInput,
  uCombinators.IResult,
  uImp_Lexer,
  System.SysUtils,
  System.Rtti;

type
  TParser = record
  private
    FCombinator: ICombinator;
    function GetResult(ATokenArray: TValue): IResult;
  public
    class operator Implicit(AParser: ICombinator): TParser;
    class operator Explicit(AParser: ICombinator): TParser;
    class operator Implicit(AParser: TParser): ICombinator;
    class operator Explicit(AParser: TParser): ICombinator;
    class operator Implicit(AParser: TParser): TParserFunc;
    class operator Explicit(AParser: TParser): TParserFunc;

    class operator Add(const ALeft, ARight: TParser): TParser;
    class operator Multiply(const ALeft, ARight: TParser): TParser;
    class operator BitwiseOr(const ALeft, ARight: TParser): TParser;
    class operator BitwiseXor(const AParser: TParser; AFunction: TFunc<TValue,TValue>): TParser;

    class operator Add(const AParser: TParser; const ADescription: string): TParser;
    property Result[ATokenArray: TValue]: IResult read GetResult; default;
  end;

function Tag(const ATag: TTokenTag): TParser;
function Reserved(const AValue: string; const ATag: TTokenTag): TParser;
function Concat(const ALeft, ARight: TParser): TParser;
function Exp(const AParser, ASeparator: TParser): TParser;
function Alternate(const ALeft, ARight: TParser): TParser;
function Opt(const AParser: TParser): TParser;
function Rep(const AParser: TParser): TParser;
function Process(const AParser: TParser; AFunction: TFunc<TValue,TValue>): TParser;
function Lazy(AParserConstructor: TFunc<TParserFunc>): TParser; overload;
function Phrase(const AParser: TParser): TParser;

implementation

uses
  System.Generics.Collections,
  uCombinators;

type
  TCache = class(TCombinatorsCache)
  private
    FCache: TObjectList<TObject>;
  public
    procedure Add(const ACombinator: TObject); override;
    procedure Clean; override;

    constructor Create; override;
    destructor Destroy; override;
  end;

  TConfig = class(TCombinatorConfig)
  public
    class function GetFactory: TCombinatorFactoryClass; override;
    class function GetTokenPropertyGetter: TTokenPropertyGetterClass; override;
    class function GetCombinatorsCache: TCombinatorsCacheClass; override;
  end;

  TTokenProperty = class(TTokenPropertyGetter)
  public
    class function GetTokenTag(const AToken: TValue): TValue; override;
    class function GetTokenText(const AToken: TValue): string; override;
  end;

function Tag(const ATag: TTokenTag): TParser;
begin
  Result := TConfig.GetFactory.CreateTag(TValue.From<TTokenTag>(ATag), TConfig);
end;

function Reserved(const AValue: string; const ATag: TTokenTag): TParser;
begin
  Result := TConfig.GetFactory.CreateReserved(AValue, TValue.From<TTokenTag>(ATag), TConfig);
end;

function Concat(const ALeft, ARight: TParser): TParser;
begin
  Result := TConfig.GetFactory.CreateConcatenate(ALeft, ARight, TConfig);
end;

function Exp(const AParser, ASeparator: TParser): TParser;
begin
  Result := TConfig.GetFactory.CreateExpression(AParser, ASeparator, TConfig);
end;

function Alternate(const ALeft, ARight: TParser): TParser;
begin
  Result := TConfig.GetFactory.CreateAlternate(ALeft, ARight, TConfig);
end;

function Opt(const AParser: TParser): TParser;
begin
  Result := TConfig.GetFactory.CreateOption(AParser, TConfig)
end;

function Rep(const AParser: TParser): TParser;
begin
  Result := TConfig.GetFactory.CreateRepeat(AParser, TConfig)
end;

function Process(const AParser: TParser; AFunction: TFunc<TValue,TValue>): TParser;
begin
  Result := TConfig.GetFactory.CreateProcess(AParser, AFunction, TConfig);
end;

function Lazy(AParserConstructor: TFunc<TParserFunc>): TParser; overload;
begin
  Result := TConfig.GetFactory.CreateLazy(AParserConstructor, TConfig)
end;

function Phrase(const AParser: TParser): TParser;
begin
  Result := TConfig.GetFactory.CreatePhrase(AParser, TConfig)
end;

{ TParser }

class operator TParser.Add(const ALeft, ARight: TParser): TParser;
begin
  Result := Concat(ALeft, ARight);
end;

class operator TParser.Add(const AParser: TParser; const ADescription: string): TParser;
begin
  Result := AParser;
  Result.FCombinator.Description := ADescription;
end;

class operator TParser.BitwiseOr(const ALeft, ARight: TParser): TParser;
begin
  Result := Alternate(ALeft, ARight);
end;

class operator TParser.BitwiseXor(const AParser: TParser; AFunction: TFunc<TValue,TValue>): TParser;
begin
  Result := Process(AParser, AFunction);
end;

class operator TParser.Explicit(AParser: TParser): TParserFunc;
begin
  Result :=
    function(AInput: IInput): IResult
    begin
      Result := AParser.FCombinator.Invoke(AInput);
    end;
end;

function TParser.GetResult(ATokenArray: TValue): IResult;
var
  LInput: TArray<TValue>;
begin
  if not Assigned(FCombinator) then
    raise EArgumentNilException.Create('Combinator');

  LInput :=
    (function: TArray<TValue>
    var
      LToken: TToken;
      LTokenArray: TArray<TToken>;
      LIndex: Integer;
    begin
      LTokenArray := ATokenArray.AsType<TArray<TToken>>;
      SetLength(Result, Length(LTokenArray));
      LIndex := Low(Result);
      for LToken in LTokenArray do
      begin
        Result[LIndex] := TValue.From<TToken>(LToken);
        Inc(LIndex);
      end;
    end)();

  Result := FCombinator.Invoke(TInput.Build(LInput));
end;

class operator TParser.Explicit(AParser: ICombinator): TParser;
begin
  Result.FCombinator := AParser;
end;

class operator TParser.Explicit(AParser: TParser): ICombinator;
begin
  Result := AParser.FCombinator;
end;

class operator TParser.Implicit(AParser: ICombinator): TParser;
begin
  Result.FCombinator := AParser;
end;

class operator TParser.Implicit(AParser: TParser): ICombinator;
begin
  Result := AParser.FCombinator;
end;

class operator TParser.Multiply(const ALeft, ARight: TParser): TParser;
begin
  Result := Exp(ALeft, ARight);
end;

class operator TParser.Implicit(AParser: TParser): TParserFunc;
begin
  Result :=
    function(AInput: IInput): IResult
    begin
      Result := AParser.FCombinator.Invoke(AInput);
    end;
end;

{ TConfig }

class function TConfig.GetCombinatorsCache: TCombinatorsCacheClass;
begin
  Result := TCache;
end;

class function TConfig.GetFactory: TCombinatorFactoryClass;
begin
  Result := TFactory;
end;

class function TConfig.GetTokenPropertyGetter: TTokenPropertyGetterClass;
begin
  Result := TTokenProperty;
end;

{ TTokenProperty }

class function TTokenProperty.GetTokenTag(const AToken: TValue): TValue;
begin
  Result := TValue.From<TTokenTag>(AToken.AsType<TToken>.Tag);
end;

class function TTokenProperty.GetTokenText(const AToken: TValue): string;
begin
  Result := AToken.AsType<TToken>.Text;
end;

{ TCache }

procedure TCache.Add(const ACombinator: TObject);
begin
  FCache.Add(ACombinator);
end;

procedure TCache.Clean;
begin
  FCache.Clear;
end;

constructor TCache.Create;
begin
  inherited;
  FCache := TObjectList<TObject>.Create;
end;

destructor TCache.Destroy;
begin
  FCache.Free;
  inherited;
end;

end.
