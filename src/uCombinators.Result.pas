unit uCombinators.Result;

interface

uses
  System.Rtti,
  System.SysUtils,
  uCombinators.IResult,
  uCombinators.IInput,
  System.Generics.Collections;

type
  TResult = record
  public
    class function Success(const AValue: TValue; const ARemainder: IInput): IResult; overload; static;
    class function Failure(const ARemainder: IInput; const AMessage: string; const AExpectations: TArray<String>): IResult; overload; static;
  strict private type
    TResult<T> = class(TInterfacedObject, IResult, IResult<T>)
    private
      FValue: TValue;
      FRemainder: IInput;
      FWasSuccessful: Boolean;
      FMessage: string;
      FExpectations: TArray<string>;
      function CalculateRecentlyConsumed: string;
    protected
      function GetValue: TValue;
      function GetRemainder: IInput;
      function GetWasSuccessful: Boolean;
      function GetMessage: string;
      function GetExpectations: TArray<string>;

      function GenericGetValue: T;
      function IResult<T>.GetValue = GenericGetValue;

      constructor Create(const AValue: T; const ARemainder: IInput); overload;
      constructor Create(const AValue: TValue; const ARemainder: IInput); overload;
      constructor Create(const ARemainder: IInput; const AMessage: string; const AExpectations: TArray<String>); overload;
    public
      function ToString: string; override;
    end;
  end;

implementation

uses
  uArrayHelper;

{ TResult }

class function TResult.Failure(const ARemainder: IInput; const AMessage: string;
  const AExpectations: TArray<String>): IResult;
begin
  Result := TResult<TValue>.Create(ARemainder, AMessage, AExpectations);
end;

class function TResult.Success(const AValue: TValue; const ARemainder: IInput): IResult;
begin
  Result := TResult<TValue>.Create(AValue, ARemainder);
end;

{ TResult.TResult<T> }

constructor TResult.TResult<T>.Create(const AValue: T; const ARemainder: IInput);
begin
  Create(TValue.From<T>(AValue), ARemainder);
end;

function TResult.TResult<T>.CalculateRecentlyConsumed: string;
const
  WINDOW_SIZE = 10;
var
  LTotalConsumedTokens: Integer;
  LWindowStart: Integer;
  LNumberOfRecentlyConsumedTokens: Integer;
  LResult: TArray<TValue>;
begin
  LTotalConsumedTokens := GetRemainder.Position;
  LWindowStart := LTotalConsumedTokens - WINDOW_SIZE;
  if LWindowStart < 0 then
    LWindowStart := 0;

  LNumberOfRecentlyConsumedTokens := LTotalConsumedTokens - LWindowStart;
  LResult := Copy(GetRemainder.Source, LWindowStart, LNumberOfRecentlyConsumedTokens);

  Result := '[' + TArray.Aggregate<TValue,string>(LResult,
    function(AItem: TValue): string
    begin
      Result := AItem.ToString;
    end,
    function(AResult, AItem: string): string
    begin
      Result := AResult + ', ' + AItem;
    end) + ']';
end;

constructor TResult.TResult<T>.Create(const ARemainder: IInput; const AMessage: string;
  const AExpectations: TArray<String>);
begin
  inherited Create;
  FValue := TValue.Empty;
  FRemainder := ARemainder;
  FWasSuccessful := False;
  FMessage := AMessage;
  FExpectations := AExpectations;
end;

constructor TResult.TResult<T>.Create(const AValue: TValue; const ARemainder: IInput);
begin
  inherited Create;
  FValue := AValue;
  FRemainder := ARemainder;
  FWasSuccessful := True;
  FMessage := '';
  FExpectations := TArray<string>.Create();
end;

function TResult.TResult<T>.GenericGetValue: T;
begin
  Result := GetValue.AsType<T>;
end;

function TResult.TResult<T>.GetExpectations: TArray<string>;
begin
  Result := FExpectations;
end;

function TResult.TResult<T>.GetMessage: string;
begin
  Result := FMessage;
end;

function TResult.TResult<T>.GetRemainder: IInput;
begin
  Result := FRemainder;
end;

function TResult.TResult<T>.GetValue: TValue;
begin
  if not GetWasSuccessful then
    raise EInvalidOpException.Create('No value can be computed.');

  Result := FValue;
end;

function TResult.TResult<T>.GetWasSuccessful: Boolean;
begin
  Result := FWasSuccessful;
end;

function TResult.TResult<T>.ToString: string;
var
  LExpectedMessage: string;
  LRecentlyConsumed: string;
begin
  if GetWasSuccessful then
  begin
    Result := Format('Successful parsing of %s.', [GetValue.ToString]);
    Exit;
  end;

  LExpectedMessage :=
    (function(const AExpectations: TArray<string>): string
    var
      LIndex: Integer;
    begin
      Result := '';
      if Length(AExpectations) > 0 then
      begin
        Result := Format(' expected %s', [AExpectations[Low(AExpectations)]]);
        for LIndex := 1 to High(AExpectations) do
          Result := Format('%s or %s', [Result, AExpectations[LIndex]]);
      end;
    end)(GetExpectations);

  LRecentlyConsumed := CalculateRecentlyConsumed;

  Result := Format('Parsing failure: %s;%s (%s); recently consumed: %s',
    [GetMessage, LExpectedMessage, GetRemainder.ToString, LRecentlyConsumed]);
end;

end.
