unit uLexer;

interface

uses
  System.RegularExpressions,
  System.Rtti,
  System.TypInfo,
  System.SysUtils;

type
  TToken<TTag> = record
  private
    FText: string;
    FTag: TTag;
  public
    class operator Equal(const ALeft, ARight: TToken<TTag>): Boolean;
    constructor Create(const AText: string; const ATag: TTag);
    property Text: string read FText;
    property Tag: TTag read FTag;
  end;

  TTokenExpression<TTag> = record
  private
    FPattern: string;
    FTag: TTag;
    FInclude: Boolean;
  public
    constructor Create(const APattern: string; const ATag: TTag; const AExclude: Boolean = False);
    property Pattern: string read FPattern;
    property Tag: TTag read FTag;
    property Include: Boolean read FInclude;
  end;

  BaseTokenExpressionAttribute = class(TCustomAttribute)
  private
    FPattern: string;
    FTag: TValue;
    FExclude: Boolean;
  protected
    function IsExcludedTag(const ATag: TValue): Boolean; virtual;
    function GetTypeInfo: PTypeInfo; virtual; abstract;
  public
    constructor Create(const APattern: string; const ATagOrder: Int64);
    property Pattern: string read FPattern;
    property Tag: TValue read FTag;
    property Exclude: Boolean read FExclude;
  end;

  TOnError = reference to procedure(const AError: string);

  TLexer = record
  private type
    TRegExHelper = record helper for TRegEx
    public
      procedure SetAnchoredOption;
    end;
  public
    class function BuildTokenExpressions<TTag>
      (const AExpressionsHolder: TClass): TArray<TTokenExpression<TTag>>; static;
    class function Lex<TTag>(const ACharacters: string;
      const ATokenExpressions: array of TTokenExpression<TTag>;
      const AOnError: TOnError): TArray<TToken<TTag>>; static;
  end;

implementation

uses
  System.Generics.Collections,
  System.RegularExpressionsCore,
  Winapi.Windows;

{ TToken }

constructor TToken<TTag>.Create(const AText: string; const ATag: TTag);
begin
  FText := AText;
  FTag := ATag;
end;

{ TLexer.TRegExHelper }

procedure TLexer.TRegExHelper.SetAnchoredOption;
begin
  Self.FRegEx.Options := Self.FRegEx.Options + [preAnchored];
end;

{ TTokenExpression<TTag> }

constructor TTokenExpression<TTag>.Create(const APattern: string;
  const ATag: TTag; const AExclude: Boolean);
begin
  FPattern := APattern;
  FTag := ATag;
  FInclude := not AExclude;
end;

{ TLexer }

class function TLexer.BuildTokenExpressions<TTag>(
  const AExpressionsHolder: TClass): TArray<TTokenExpression<TTag>>;
var
  LContext: TRttiContext;
  LTokenExpressions: TList<TTokenExpression<TTag>>;
  LAttribute: TCustomAttribute;
begin
  LTokenExpressions := TList<TTokenExpression<TTag>>.Create;
  LContext := TRttiContext.Create;
  try
    for LAttribute in LContext.GetType(AExpressionsHolder).GetAttributes do
    begin
      if LAttribute is BaseTokenExpressionAttribute then
        with BaseTokenExpressionAttribute(LAttribute) do
          LTokenExpressions.Add(TTokenExpression<TTag>.Create(Pattern, Tag.AsType<TTag>, Exclude));
    end;
    Result := LTokenExpressions.ToArray;
  finally
    LContext.Free;
    LTokenExpressions.Free;
  end;
end;

class function TLexer.Lex<TTag>(const ACharacters: string;
  const ATokenExpressions: array of TTokenExpression<TTag>;
  const AOnError: TOnError): TArray<TToken<TTag>>;
var
  LTokens: TList<TToken<TTag>>;
  LPos: Integer;
  LRegex: TRegEx;
  LMatch: TMatch;
  LCharLen: Integer;
  LTokenExpression: TTokenExpression<TTag>;
begin
  LPos := 1;
  LCharLen := Length(ACharacters);
  LTokens := TList<TToken<TTag>>.Create;
  try
    if LCharLen = 0 then
      Exit(LTokens.ToArray);

    while LPos <= LCharLen do
    begin
      for LTokenExpression in ATokenExpressions do
      begin
        LRegex := TRegEx.Create(LTokenExpression.Pattern);
        LRegex.SetAnchoredOption;
        LMatch := LRegex.Match(ACharacters, LPos);
        if LMatch.Success then
        begin
          if LTokenExpression.Include then
            LTokens.Add(TToken<TTag>.Create(LMatch.Value, LTokenExpression.Tag));
          Break;
        end;
      end;
      if not LMatch.Success then
      begin
        AOnError(Format('Illegal character: <%s>', [ACharacters[LPos]]));
//        Writeln('Illegal character: <', ACharacters[LPos], '>');
        Exit;
      end
      else
        with LMatch do
          LPos := Index + Length;
    end;
    Result := LTokens.ToArray;
  finally
    LTokens.Free;
  end;
end;

{ BaseTokenExpressionAttribute }

constructor BaseTokenExpressionAttribute.Create(const APattern: string;
  const ATagOrder: Int64);
begin
  inherited Create;
  FPattern := APattern;
  FTag := TValue.FromOrdinal(GetTypeInfo, ATagOrder);
  FExclude := IsExcludedTag(FTag);
end;

function BaseTokenExpressionAttribute.IsExcludedTag(const ATag: TValue): Boolean;
begin
  Result := False;
end;

class operator TToken<TTag>.Equal(const ALeft, ARight: TToken<TTag>): Boolean;
begin
  Result := (ALeft.Text = ARight.Text)
    and (TValue.From<TTag>(ALeft.Tag).AsOrdinal = TValue.From<TTag>(ARight.Tag).AsOrdinal);
end;

end.
