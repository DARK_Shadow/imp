unit uImp_Ast.ExecutionEngine;

interface

uses
  uImp_Ast,
  uAst.Interfaces,
  System.Generics.Collections,
  System.TypInfo,
  System.Rtti,
  System.SysUtils;

type
  TCompiledStatement = reference to procedure(const AEnvironment: IEnvironment);

  IExecutionEngine = interface
  ['{E90B949A-92F3-400E-9475-ABF67196931E}']
    function Compile(const AStatement: IStatement): TCompiledStatement;
    function Execute(const AEnvironment: IEnvironment): TValue;
  end;

  TAbstractExecutionEngine = class abstract(TInterfacedObject, IExecutionEngine, INodeVisitor)
  protected
    procedure Visit(const ANode: IAssignStatement); overload; virtual;
    procedure Visit(const ANode: ICompoundStatement); overload; virtual;
    procedure Visit(const ANode: IIfStatement); overload; virtual;
    procedure Visit(const ANode: IWhileStatement); overload; virtual;
    procedure Visit(const ANode: IIntegerExpression); overload; virtual;
    procedure Visit(const ANode: IVariableExpression); overload; virtual;
    procedure Visit(const ANode: IBinaryExpression); overload; virtual;
    procedure Visit(const ANode: IUnaryExpression); overload; virtual;

    function Compile(const AStatement: IStatement): TCompiledStatement; virtual; abstract;
    function Execute(const AEnvironment: IEnvironment): TValue; virtual; abstract;
  end;

  TOperatorInfo = record
  strict private
    FOp: string;
    FLeftTypeInfo: PTypeInfo;
    FRightTypeInfo: PTypeInfo;
  public
    constructor Create(const AOperator: string; const ALeftTypeInfo, ARightTypeInfo: PTypeInfo);
    class operator Equal(const ALeft, ARight: TOperatorInfo): Boolean;
    class function Build<TLeft,TRight>(const AOperator: string): TOperatorInfo; static; inline;
    property Op: string read FOp;
    property LeftTypeInfo: PTypeInfo read FLeftTypeInfo;
    property RightTypeInfo: PTypeInfo read FRightTypeInfo;
  end;

  TBinaryOperators = class
  strict private type
    TBinaryOperatorFunc = TFunc<TValue,TValue,TValue>;
    TOperatorPair = TPair<TOperatorInfo,TBinaryOperatorFunc>;
    TOperatorsInfoList = TList<TOperatorPair>;
  strict private
    FOperatorsInfoList: TOperatorsInfoList;
    function GetOperator(const AOperator: string; const ALeftTypeInfo, ARightTypeInfo: PTypeInfo): TBinaryOperatorFunc;
    function OperatorPairCompare(const ALeft, ARight: TOperatorPair): Integer;
  public
    constructor Create;
    destructor Destroy; override;
    procedure AddOperator<TLeft;TRight;TResult>(const AOperator: string; AOperatorFunc: TFunc<TLeft,TRight,TResult>);
    procedure RegisterOperators(const AClass: TClass);
    property &Operator[const AOperator: string; const ALeftTypeInfo, ARightTypeInfo: PTypeInfo]: TBinaryOperatorFunc read GetOperator;
  end;

  TExecutor = class(TAbstractExecutionEngine, INodeVisitor)
  private type
    TValues = TDictionary<TGUID,TValue>;
    TStatementsList = TDictionary<TGUID,TCompiledStatement>;
    TLines = TList<TGUID>;
  private
    FValues: TValues;
    FStatementsList: TStatementsList;
    FLines: TLines;
    FCompiled: TCompiledStatement;
    FBinaryOperators: TBinaryOperators;
    procedure CompileStatement(const ANode: INode; const AStatement: TCompiledStatement);
  protected
    procedure Visit(const ANode: IAssignStatement); overload; override;
    procedure Visit(const ANode: ICompoundStatement); overload; override;
    procedure Visit(const ANode: IIfStatement); overload; override;
    procedure Visit(const ANode: IWhileStatement); overload; override;
    procedure Visit(const ANode: IIntegerExpression); overload; override;
    procedure Visit(const ANode: IVariableExpression); overload; override;
    procedure Visit(const ANode: IBinaryExpression); overload; override;
    procedure Visit(const ANode: IUnaryExpression); overload; override;

    function Compile(const AStatement: IStatement): TCompiledStatement; override;
    function Execute(const AEnvironment: IEnvironment): TValue; override;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  System.Generics.Defaults;

type
  BinaryOperatorAttribute = class(TCustomAttribute)
  strict private
    FOperator: string;
  public
    constructor Create(const AOperator: string);
    property &Operator: string read FOperator;
  end;

  TIntegerBinaryOperatorHandler = class abstract
  public
    [BinaryOperator('+')]
    class function Add(ALeft, ARight: Integer): Integer; static;
    [BinaryOperator('-')]
    class function Subtract(ALeft, ARight: Integer): Integer; static;
    [BinaryOperator('*')]
    class function Multiply(ALeft, ARight: Integer): Integer; static;
    [BinaryOperator('>')]
    class function GreaterThan(ALeft, ARight: Integer): Boolean; static;
  end;

{ TAbstractExecutionEngine }

procedure TAbstractExecutionEngine.Visit(const ANode: IIfStatement);
begin
  ANode.Condition.Accept(Self);
  ANode.TrueStatement.Accept(Self);
  ANode.FalseStatemtnt.Accept(Self);
end;

procedure TAbstractExecutionEngine.Visit(const ANode: IWhileStatement);
begin
  ANode.Condition.Accept(Self);
  ANode.Body.Accept(Self);
end;

procedure TAbstractExecutionEngine.Visit(const ANode: IAssignStatement);
begin
  ANode.Expression.Accept(Self);
end;

procedure TAbstractExecutionEngine.Visit(const ANode: ICompoundStatement);
begin
  ANode.First.Accept(Self);
  ANode.Second.Accept(Self);
end;

procedure TAbstractExecutionEngine.Visit(const ANode: IBinaryExpression);
begin
  ANode.Left.Accept(Self);
  ANode.Right.Accept(Self);
end;

procedure TAbstractExecutionEngine.Visit(const ANode: IUnaryExpression);
begin
  ANode.Expression.Accept(Self);
end;

procedure TAbstractExecutionEngine.Visit(const ANode: IIntegerExpression);
begin
end;

procedure TAbstractExecutionEngine.Visit(const ANode: IVariableExpression);
begin
end;

{ TExecutor }

function TExecutor.Compile(const AStatement: IStatement): TCompiledStatement;
begin
  AStatement.Accept(Self);
  Result :=
    procedure(const AEnvironment: IEnvironment)
    var
      LNode: TGUID;
    begin
      for LNode in FLines do
        FStatementsList[LNode](AEnvironment);
    end;
  FCompiled := Result;
end;

procedure TExecutor.CompileStatement(const ANode: INode;
  const AStatement: TCompiledStatement);
begin
  FStatementsList.Add(ANode.GUID, AStatement);
  FLines.Add(ANode.GUID);
end;

constructor TExecutor.Create;
begin
  inherited Create;
  FValues := TValues.Create;
  FStatementsList := TStatementsList.Create;
  FLines := TLines.Create;
  FBinaryOperators := TBinaryOperators.Create;

  FBinaryOperators.
    AddOperator<Integer,Integer,Integer>('+', TIntegerBinaryOperatorHandler.Add);

  FBinaryOperators.
    AddOperator<Integer,Integer,Integer>('-', TIntegerBinaryOperatorHandler.Subtract);

  FBinaryOperators.
    AddOperator<Integer,Integer,Integer>('*', TIntegerBinaryOperatorHandler.Multiply);

  FBinaryOperators.
    AddOperator<Integer,Integer,Boolean>('>', TIntegerBinaryOperatorHandler.GreaterThan);
end;

destructor TExecutor.Destroy;
begin
  FBinaryOperators.Free;
  FLines.Free;
  FStatementsList.Free;
  FValues.Free;
  inherited;
end;

function TExecutor.Execute(const AEnvironment: IEnvironment): TValue;
begin
  if Assigned(FCompiled) then
    FCompiled(AEnvironment);
end;

procedure TExecutor.Visit(const ANode: IIfStatement);
begin
  inherited;

end;

procedure TExecutor.Visit(const ANode: ICompoundStatement);
begin
  inherited;

  CompileStatement(ANode,
    procedure(const AEnvironment: IEnvironment)
    begin
      FStatementsList[ANode.First.GUID](AEnvironment);
      FStatementsList[ANode.Second.GUID](AEnvironment);
    end);
end;

procedure TExecutor.Visit(const ANode: IAssignStatement);
begin
  inherited;

  CompileStatement(ANode,
      procedure(const AEnvironment: IEnvironment)
      begin
        FStatementsList[ANode.Expression.GUID](AEnvironment);

        AEnvironment[ANode.Name] := FValues[ANode.Expression.GUID];
      end);
end;

procedure TExecutor.Visit(const ANode: IWhileStatement);
begin
  inherited;

  CompileStatement(ANode,
      procedure(const AEnvironment: IEnvironment)
      var
        LCondition: TValue;
      begin
        FStatementsList[ANode.Condition.GUID](AEnvironment);
        LCondition := FValues[ANode.Condition.GUID];

        while LCondition.AsBoolean do
        begin
          FStatementsList[ANode.Body.GUID](AEnvironment);

          FStatementsList[ANode.Condition.GUID](AEnvironment);
          LCondition := FValues[ANode.Condition.GUID];
        end;
      end);
end;

procedure TExecutor.Visit(const ANode: IBinaryExpression);
begin
  inherited;

  CompileStatement(ANode,
    procedure(const AEnvironment: IEnvironment)
    var
      LLeft: TValue;
      LRight: TValue;
      LOperation: TFunc<TValue,TValue,TValue>;
    begin
      FStatementsList[ANode.Left.GUID](AEnvironment);
      FStatementsList[ANode.Right.GUID](AEnvironment);

      LLeft := FValues[ANode.Left.GUID];
      LRight := FValues[ANode.Right.GUID];

      LOperation := FBinaryOperators.Operator[ANode.&Operator.AsString, LLeft.TypeInfo, LRight.TypeInfo];
      FValues.AddOrSetValue(ANode.GUID, LOperation(LLeft, LRight));
    end);
end;

procedure TExecutor.Visit(const ANode: IUnaryExpression);
begin
  inherited;

end;

procedure TExecutor.Visit(const ANode: IIntegerExpression);
begin
  inherited;

  CompileStatement(ANode,
    procedure(const AEnvironment: IEnvironment)
    begin
      if not FValues.ContainsKey(ANode.GUID) then
        FValues.Add(ANode.GUID, ANode.Value);
    end);
end;

procedure TExecutor.Visit(const ANode: IVariableExpression);
begin
  inherited;

  CompileStatement(ANode,
    procedure(const AEnvironment: IEnvironment)
    begin
      FValues.AddOrSetValue(ANode.GUID, AEnvironment[ANode.Name]);
    end);
end;

{ TOperatorInfo }

class function TOperatorInfo.Build<TLeft, TRight>(
  const AOperator: string): TOperatorInfo;
begin
  Result := TOperatorInfo.Create(AOperator, TypeInfo(TLeft), TypeInfo(TRight));
end;

constructor TOperatorInfo.Create(const AOperator: string;
  const ALeftTypeInfo, ARightTypeInfo: PTypeInfo);
begin
  FOp := AOperator;
  FLeftTypeInfo := ALeftTypeInfo;
  FRightTypeInfo := ARightTypeInfo;
end;

class operator TOperatorInfo.Equal(const ALeft,
  ARight: TOperatorInfo): Boolean;
begin
  with ALeft do
    Result := SameText(Op, ARight.Op)
      and (LeftTypeInfo = ARight.LeftTypeInfo)
      and (RightTypeInfo = ARight.RightTypeInfo);
end;

{ TBinaryOperators }

procedure TBinaryOperators.AddOperator<TLeft, TRight, TResult>(
  const AOperator: string; AOperatorFunc: TFunc<TLeft, TRight, TResult>);
var
  LOpInfo: TOperatorInfo;
begin
  LOpInfo := TOperatorInfo.Build<TLeft,TRight>(AOperator);

  FOperatorsInfoList.Add(TOperatorPair.Create(LOpInfo,
    function(Left, Right: TValue): TValue
    begin
      Result := TValue.From<TResult>(AOperatorFunc(Left.AsType<TLeft>, Right.AsType<TRight>));
    end));

  FOperatorsInfoList.Sort;
end;

constructor TBinaryOperators.Create;
begin
  inherited;
  FOperatorsInfoList := TOperatorsInfoList.Create(
    TComparer<TOperatorPair>.Construct(OperatorPairCompare));
end;

destructor TBinaryOperators.Destroy;
begin
  FOperatorsInfoList.Free;
  inherited;
end;

function TBinaryOperators.GetOperator(
  const AOperator: string; const ALeftTypeInfo, ARightTypeInfo: PTypeInfo): TBinaryOperatorFunc;

  function GenerateError(const AInfo: TOperatorInfo): TFunc<TValue, TValue, TValue>;
  var
    LInfo: TOperatorInfo;
  begin
    LInfo := AInfo;
    Result :=
      function(ALeft, ARight: TValue): TValue
      begin
        raise Exception.CreateFmt('Operator "%s" not implemented.', [LInfo.Op]);
      end;
  end;

var
  LIndex: Integer;
  LOperatorInfo: TOperatorInfo;
begin
  LOperatorInfo := TOperatorInfo.Create(AOperator, ALeftTypeInfo, ARightTypeInfo);
  if FOperatorsInfoList.BinarySearch(TOperatorPair.Create(LOperatorInfo, nil), LIndex) then
    Result := FOperatorsInfoList[LIndex].Value
  else
    Result := GenerateError(LOperatorInfo);
end;

function TBinaryOperators.OperatorPairCompare(const ALeft,
  ARight: TOperatorPair): Integer;
begin
  Result :=
    Ord(ALeft.Key.LeftTypeInfo.Kind) - Ord(ARight.Key.LeftTypeInfo.Kind)
    + Ord(ALeft.Key.RightTypeInfo.Kind) - Ord(ARight.Key.RightTypeInfo.Kind)
    + CompareText(ALeft.Key.Op, ARight.Key.Op);
end;

procedure TBinaryOperators.RegisterOperators(const AClass: TClass);
//type
//  TBinaryOperator = function(ALeft, ARight: Integer): Integer;
//var
//  LMethod: TRttiMethod;
//  LAttribute: TCustomAttribute;
//  LLeftTypeInfo: PTypeInfo;
//  LRightTypeInfo: PTypeInfo;
//  LResultTypeInfo: PTypeInfo;
begin
//  with TRttiContext.Create do
//  try
//    for LMethod in GetType(AClass).GetMethods do
//    begin
//      if LMethod.IsStatic and LMethod.IsClassMethod then
//        for LAttribute in LMethod.GetAttributes do
//        begin
//          if LAttribute is BinaryOperatorAttribute then
//          begin
//            LLeftTypeInfo := LMethod.GetParameters[0].ParamType.Handle;
//            LRightTypeInfo := LMethod.GetParameters[1].ParamType.Handle;
//            LResultTypeInfo := LMethod.ReturnType.Handle;
//
//            FOperatorsInfoList.Add(
//              TOperatorPair.Create(
//                TOperatorInfo.Create(BinaryOperatorAttribute(LAttribute).&Operator,
//                  LLeftTypeInfo, LRightTypeInfo),
//                  TBinaryOperator(LMethod.CodeAddress)));
//          end;
//        end;
//    end;
//  finally
//    Free;
//  end;
end;

{ TIntegerBinaryOperatorHandler }

class function TIntegerBinaryOperatorHandler.Add(ALeft,
  ARight: Integer): Integer;
begin
  Result := ALeft + ARight;
end;

class function TIntegerBinaryOperatorHandler.GreaterThan(ALeft,
  ARight: Integer): Boolean;
begin
  Result := ALeft > ARight;
end;

class function TIntegerBinaryOperatorHandler.Multiply(ALeft,
  ARight: Integer): Integer;
begin
  Result := ALeft * ARight;
end;

class function TIntegerBinaryOperatorHandler.Subtract(ALeft,
  ARight: Integer): Integer;
begin
  Result := ALeft - ARight;
end;

{ BinaryOperatorAttribute }

constructor BinaryOperatorAttribute.Create(const AOperator: string);
begin
  inherited Create;
  FOperator := AOperator;
end;

end.
