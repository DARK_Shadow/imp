unit uCombinators.IInput;

interface

uses
  System.Generics.Collections,
  System.Rtti;

type
  IInput = interface
    function Advance: IInput;
    function GetSource: TArray<TValue>;
    function GetCurrent: TValue;
    function GetAtEnd: Boolean;
    function GetPosition: Integer;
    function GetLine: Integer;
    function GetColumn: Integer;
    function GetMemos: TDictionary<TObject,TObject>;
    function ToString: string;

    property Source: TArray<TValue> read GetSource;
    property Current: TValue read GetCurrent;
    property AtEnd: Boolean read GetAtEnd;
    property Position: Integer read GetPosition;
    property Line: Integer read GetLine;
    property Column: Integer read GetColumn;
    property Memos: TDictionary<TObject,TObject> read GetMemos;
  end;

//  IInput<T> = interface(IInput)
//    function Advance: IInput<T>;
//    function GetSource: TArray<T>;
//    function GetCurrent: T;
//
//    property Source: TArray<T> read GetSource;
//    property Current: T read GetCurrent;
//  end;

  TInput = record
  public
    class function Build(const ASource: TArray<TValue>): IInput; static;
  end;

implementation

uses
  uCombinators.Input;

{ TInput }

class function TInput.Build(const ASource: TArray<TValue>): IInput;
begin
  Result := uCombinators.Input.TInput.Create(ASource);
end;

end.
