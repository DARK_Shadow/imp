unit uCombinators.IResult;

interface

uses
  System.Rtti,
  uCombinators.IInput;

type
  IResult = interface
    function GetValue: TValue;
    function GetRemainder: IInput;
    function GetWasSuccessful: Boolean;
    function GetMessage: string;
    function GetExpectations: TArray<string>;

    function ToString: string;

    property Value: TValue read GetValue;
    property Remainder: IInput read GetRemainder;
    property WasSuccessful: Boolean read GetWasSuccessful;
    property Message: string read GetMessage;
    property Expectations: TArray<string> read GetExpectations;
  end;

  IResult<TOut> = interface(IResult)
    function GetValue: TOut;

    property Value: TOut read GetValue;
  end;

implementation

end.
