unit uImp_Lexer;

interface

uses
  uLexer;

type
  TOnError = uLexer.TOnError;

  TTokenTag = (ttNone, ttReserved, ttInt, ttId);

  TTokenTagHelper = record helper for TTokenTag
  private
    const TagString: array[TTokenTag] of string = ('None', 'Reserved', 'Int', 'Id');
  public
    function ToString: string;
  end;

  TToken = TToken<TTokenTag>;

function Imp_Lex(const ACharacters: string; const AOnError: TOnError): TArray<TToken>;

implementation

uses
  System.TypInfo,
  System.Rtti;

type
  TokenExpressionAttribute = class(BaseTokenExpressionAttribute)
  protected
    function IsExcludedTag(const ATag: TValue): Boolean; override;
    function GetTypeInfo: PTypeInfo; override;
  end;

  [TokenExpression('[ \n\t\r]+', Ord(ttNone))]
  [TokenExpression('#[^\n]*', Ord(ttNone))]
  [TokenExpression('\:=', Ord(ttReserved))]
  [TokenExpression('\(', Ord(ttReserved))]
  [TokenExpression('\)', Ord(ttReserved))]
  [TokenExpression(';', Ord(ttReserved))]
  [TokenExpression('\+', Ord(ttReserved))]
  [TokenExpression('-', Ord(ttReserved))]
  [TokenExpression('\*', Ord(ttReserved))]
  [TokenExpression('/', Ord(ttReserved))]
  [TokenExpression('<', Ord(ttReserved))]
  [TokenExpression('<=', Ord(ttReserved))]
  [TokenExpression('>', Ord(ttReserved))]
  [TokenExpression('>=', Ord(ttReserved))]
  [TokenExpression('=', Ord(ttReserved))]
  [TokenExpression('!=', Ord(ttReserved))]
  [TokenExpression('and', Ord(ttReserved))]
  [TokenExpression('or', Ord(ttReserved))]
  [TokenExpression('not', Ord(ttReserved))]
  [TokenExpression('if', Ord(ttReserved))]
  [TokenExpression('then', Ord(ttReserved))]
  [TokenExpression('else', Ord(ttReserved))]
  [TokenExpression('while', Ord(ttReserved))]
  [TokenExpression('do', Ord(ttReserved))]
  [TokenExpression('end', Ord(ttReserved))]
  [TokenExpression('[0-9]+', Ord(ttInt))]
  [TokenExpression('[A-Za-z][A-Za-z0-9_]*', Ord(ttId))]
  TExpressionHolder = class(TObject);

function Imp_Lex(const ACharacters: string; const AOnError: TOnError): TArray<TToken>;
begin
  Result := TLexer.Lex<TTokenTag>(ACharacters,
    TLexer.BuildTokenExpressions<TTokenTag>(TExpressionHolder),
    AOnError);
end;

{ TTokenTagHelper }

function TTokenTagHelper.ToString: string;
begin
  Result := TagString[Self];
end;

{ TokenExpressionAttribute }

function TokenExpressionAttribute.GetTypeInfo: PTypeInfo;
begin
  Result := TypeInfo(TTokenTag);
end;

function TokenExpressionAttribute.IsExcludedTag(const ATag: TValue): Boolean;
begin
  Result := ATag.AsType<TTokenTag> = ttNone;
end;

end.
